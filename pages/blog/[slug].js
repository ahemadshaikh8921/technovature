
import Footer3 from '../../layout/footer-3';
import Header3 from '../../layout/header-3';
import Seo from "../../element/seo";
import { db, storage } from "../../firebase_config"
import ReactHtmlParser from "react-html-parser"
import { MDXRemote } from 'next-mdx-remote'
import SyntaxHighlighter from 'react-syntax-highlighter'
import { serialize } from 'next-mdx-remote/serialize'
import matter from 'gray-matter'
import React from "react"
import Markdown from 'markdown-to-jsx';
import { useState, useEffect } from "react"
import Link from 'next/dist/client/link';

function ServicesDetails3({ post, }) {
  const [blogData, setBlogData] = useState([]);

  useEffect(() => {
    getData();
  }, []); // blank to run only on first launch

  function getData() {
    db.collection("blog").onSnapshot(function (querySnapshot) {
      setBlogData(
        querySnapshot.docs.map((doc) => ({
          title: doc.data().title,
          author: doc.data().author,
          content: doc.data().content,
          // featured_img: storage.ref(doc.data().featured_img) //name in storage in firebase console
          //     .getDownloadURL()
          //     .then((url) => { ImgStoreFunc(url); }).catch((e) => console.log('Errors while downloading => ', e)),
          featured_img: doc.data().featured_img,
          // featured_img: getImg(doc.data().featured_img),
          metadesc: doc.data().metadesc,
          slug: doc.data().slug,
          keywords: doc.data().keywords,
          date: doc.data().created_at,
        }))
      );
    });
  }

  console.log(blogData, 'blogData');

  const data = post[0]

  // const [imageUrl, setImageUrl] = useState(undefined);

  // useEffect(() => {
  //   storage.ref(`${data.featured_img}`) //name in storage in firebase console
  //     .getDownloadURL()
  //     .then((url) => {
  //       setImageUrl(url);
  //     })
  //     .catch((e) => console.log('Errors while downloading => ', e));
  // }, []);
  // console.log("imageUrl", imageUrl)
  const SeoData = {
    title: `${data.title} | Technovature Software Solutions Pvt. Ltd.`,
    desc: `${data.metadesc}`,
    // canonical: `${canonical}`
  }
  const content = data.content
  console.log(data.metadesc, "data is here")
  return (
    <>
      <Seo {...SeoData} />
      <Header3 />
      <div style={{ background: "#f2f2f2" }} className="page-content">
        {/* <!-- Banner  --> */}
        <div className="dlab-bnr-inr style-1 bg-primary" style={{ "backgroundImage": "url(`images/banner/bnr2.png`), var(--gradient-sec)", "backgroundSize": "cover, 200%" }}>
          <div className="container">
            <div className="dlab-bnr-inr-entry">
              <h1>{data.title}</h1>
            </div>
          </div>
        </div>
        {/* <!-- Banner End --> */}
        <section className='container content-inner' id='blog_pages'>
          <div className="row">
            <div style={{ background: "white", padding: "0px 50px 25px 40px", borderRadius: "5px" }} className='col-xl-8 col-lg-8 m-b50'>
              <div className=' list-style heading-style img-style'>
                <div className="dlab-media m-b30 rounded-md ">
                  <img src={data.featured_img} alt="" />
                </div>
              </div>
              <Markdown>{content}</Markdown>
            </div>

            <div className="col-xl-4 col-lg-4 m-b50">
              <aside style={{ background: "white", padding: "20px 30px", borderRadius: "5px", zIndex: "100" }} className="side-bar sticky-top">
                {/* <div className="widget style-1">
                    <div className="search-bx style-1">
                      <form role="search" method="post">
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <span className="input-group-text"><i className="la la-search"></i></span>
                          </div>
                          <input name="text" className="form-control" placeholder="Search" type="text" />
                          <span className="input-group-btn">
                            <button type="submit" className="btn btn-primary gradient"><i className="la la-long-arrow-right"></i></button>
                          </span>
                        </div>
                      </form>
                    </div>
                  </div> */}
                {/* <div className="widget widget_archive style-1">
                    <h2 className="widget-title">Category</h2>
                    <ul>
                      <li><a href="/services-3">Design<span>05</span></a></li>
                      <li><a href="/services-3">Development<span>25</span></a></li>
                      <li><a href="/services-3">SEO<span>20</span></a></li>
                      <li><a href="/services-3">App Design<span>08</span></a></li>
                      <li><a href="/services-3">Branding<span>22</span></a></li>
                    </ul>
                  </div> */}
                <div className="widget recent-posts-entry style-1">
                  <h2 className="widget-title">Recent Posts</h2>
                  <div className="widget-post-bx">
                    {data && blogData.map((blog, id) => {
                      const a = "/blog/"+blog.slug;
                      {/* let d = new Date(1970, 0, 1);
                      d.setSeconds(blog.date.seconds); */}
                       return <div id={id} className="widget-post clearfix">
                      <div className="dlab-media">
                        <Link href={a} ><a><img src={blog.featured_img} alt="" /></a></Link>
                      </div>
                      <div className="dlab-info">
                        <h4 className="title"><Link href={a}><a>{blog.title}</a></Link></h4>
                        <div className="dlab-meta">
                          <ul>
                            <li className="post-date">{new Date(blog.date.seconds * 1000).toDateString()}</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    })}
                  </div>
                </div>
                {/* <div className="widget widget_archive style-1">
                    <h2 className="widget-title">Archives</h2>
                    <ul>
                      <li><a href="/services-3">January<span>05</span></a></li>
                      <li><a href="/services-3">Fabruary<span>25</span></a></li>
                      <li><a href="/services-3">March<span>20</span></a></li>
                      <li><a href="/services-3">April<span>08</span></a></li>
                      <li><a href="/services-3">May<span>22</span></a></li>
                      <li><a href="/services-3">Jun<span>11</span></a></li>
                      <li><a href="/services-3">July<span>19</span></a></li>
                    </ul>
                  </div> */}
                {/* <div className="widget widget_tag_cloud style-1">
                    <h2 className="widget-title">Tags</h2>
                    <div className="tagcloud">
                      <a href="#">Business</a>
                      <a href="#">News</a>
                      <a href="#">Brand</a>
                      <a href="#">Website</a>
                      <a href="#">Internal</a>
                      <a href="#">Strategy</a>
                      <a href="#">Brand</a>
                      <a href="#">Mission</a>
                    </div>
                  </div> */}
              </aside>
            </div>
            {/* </div> */}
            {/* </div> */}
          </div>
        </section>
        {/* <!-- Services Details --> */}
      </div>
      <Footer3 />
    </>
  )
}

export default ServicesDetails3;

// export async function getServerSideProps({params:{slug}}) {
//   console.log("slug",slug)
//  return {
//    props: {

//    },
//  }
// }

export const getStaticPaths = async () => {
  let posts = []
  const ref = db.collection('blog')
  try {
    let allPosts = await ref.get()
    for (const doc of allPosts.docs) {
      // console.log(doc.id, '=>', doc.data())
      // console.log('the id is....')

      let data = doc.data()
      console.log(data.slug)
      posts.push({
        slug: data.slug,
      })
    }
  } catch (e) {
    console.log(e)
  }
  // Get the paths we want to pre-render based on posts
  const paths = posts.map((post) => ({
    params: { slug: post.slug },
  }))

  return { paths, fallback: false }
}

export const getStaticProps = async (context) => {
  const id = context.params.slug

  // console.log('id is')
  // console.log(id)

  let post = []

  try {
    const ref = db.collection('blog').where('slug', '==', id)

    const qsnapshot = await ref.get()
    qsnapshot.forEach((doc) => {
      const data = doc.data()
      // console.log('the title is...')
      // console.log(data.title)
      delete data['created_at'];
      // console.log(data)
      post.push(data)
    })
    return {
      props: { post: post },
      revalidate: 1,
    }
  } catch (e) {
    console.log(e)
  }
  const { data: content } = matter(post[0].contentDemo)
  const mdxSource = await serialize(content)
  return {
    props: { post: post, mdxSource },
    revalidate: 1,
  }
}

