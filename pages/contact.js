import Link from "next/link";
import Quote3 from "../element/quote-3";
import Footer3 from "../layout/footer-3";
import Header3 from "../layout/header-3";
import GetInTouch3 from "../element/get-in-touch";
import Location from "../element/location";
import Seo from "../element/seo";
import {SeoData,LocationData} from "../content/contact";

function ContactUs1() {
  return (
    <>
   <Seo title={SeoData.title} desc={SeoData.desc} canonical={SeoData.canonical}/>
      <Header3 />
      <div className="page-content bg-white">
        {/* <!-- Banner  --> */}
        {/* <div
          className="dlab-bnr-inr style-1 bg-primary"
          style={{
            backgroundImage: "url(images/banner/bnr2.png), var(--gradient-sec)",
            backgroundSize: "cover, 200%",
          }}
        >
          <div className="container">
            <div className="dlab-bnr-inr-entry">
              <h1>Contact Us</h1>
              <nav aria-label="breadcrumb" className="breadcrumb-row style-1">
                <ul className="breadcrumb">
                  <li className="breadcrumb-item">
                    <Link href="/">
                      <a>Home</a>
                    </Link>
                  </li>
                  <li className="breadcrumb-item active" aria-current="page">
                    Contact Us
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div> */}
        {/* <!-- Banner End --> */}

        {/* <!-- Get A Quote --> */}
        <GetInTouch3 />
        <Location reverse {...LocationData}/>
      </div>
      <Footer3 />
    </>
  );
}

export default ContactUs1;
