

import Footer3 from '../layout/footer-3';
import Header3 from '../layout/header-3';
import { useState } from "react";
import Link from 'next/link';
import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'
import Seo from "../element/seo"

function Services3({posts}) {
    const [open, setOpen] = useState("p2")
	let openData = ["p1","p2","p3"]
	const SeoData = {
        title: 'Services | Technovature Software Solutions Pvt. Ltd.',
        desc: 'High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation',
        canonical: 'https://www.technovature.com/services/'
    }
  return (
    <>
	<Seo {...SeoData} />
    <Header3/>
    <div className="page-content bg-white">
		{/* <!-- Banner  --> */}
		<div className="dlab-bnr-inr style-1 bg-primary" style={{"backgroundImage":"url(images/banner/bnr2.png), var(--gradient-sec)","backgroundSize":"cover, 200%"}}>
			<div className="container">
				<div className="dlab-bnr-inr-entry">
					<h1>Services</h1>
					
				</div>
			</div>
		</div>
		 {/* <!-- Banner End -->  */}
		
		 {/* <!-- Services --> */}
		 <section className="content-inner">
			<div className="container">
				 <div className="section-head style-3 text-center mb-4">
					<h2 className="title">Our Speciallization</h2>
					<div className="dlab-separator style-2 bg-primary"></div>
					<p style={{ marginBottom: "15px" }}>Your future is bright and boundless with an Innovation driven technology blue printand a partner that delivers it.</p>
				</div>
				<div className="row">
					{posts.map((x,i)=><div key={x.title + i} className="col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay={"0.2s"}>
						<div className={`${open === [openData[i]] ? "icon-bx-wraper style-5 dlab-blog style-1  box-hover text-center m-b30 active" : "icon-bx-wraper style-5 dlab-blog style-1  box-hover text-center m-b30"}`} onMouseOver={() => setOpen("p1")}>
							<div className="icon-bx"> 
								<span className="icon-cell"><i><img style={{width:"40%"}} src={x.frontMatter.icon}/></i></span> 
							</div>
							<div className="icon-content">
								<h3 style={{fontSize:"1.5rem"}} className="dlab-title m-b15">{x.frontMatter.title}</h3>
								<p className="m-b20">{x.frontMatter.metadesc}</p>
								<Link href={`/services/${x.frontMatter.slug}`}><a className="btn btn-outline-primary"><i className="fa fa-angle-right"></i></a></Link>
							</div>
						</div>
					</div>
					)}
				
				</div>			
			</div>
		</section>
	</div>
     <Footer3/>
    </>
  )
}

export default Services3;


export const getStaticProps = async () => {
    const files = fs.readdirSync(path.join('content/services'))
    const posts = files.map(filename => {
        const markdownWithMeta = fs.readFileSync(path.join('content/services', filename), 'utf-8')
        const { data: frontMatter } = matter(markdownWithMeta)
        return {
            frontMatter,
            slug: filename.split('.')[0]
        }
    })
    return {
        props: {
            posts
        }
    }
}