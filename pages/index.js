import { useEffect, useState } from "react";
import AboutUs3 from "../element/aboutUs-3";
import GetInTouch3 from "../element/get-in-touch";
import Projects3 from "../element/Projects-3";
import Service3 from "../element/service-3";
import Slider3 from "../element/slider-3";
import Team3 from "../element/team-3";
import Blog3 from "./../element/blog-3";
import Footer3 from "./../layout/footer-3";
import Header3 from "./../layout/header-3";
import Seo from "../element/seo";
import { db, } from "../firebase_config"
import Link from "next/link"
import { BannerData, ServiceSection, AboutUsData, AboutUsData2, portfolioData, TeamData, SeoData } from "../content/home"
import { LazyLoadImage } from "react-lazy-load-image-component";

function Index3({ }) {

  const [blogData, setBlogData] = useState([]);
  useEffect(() => {
    getData();
  }, []); // blank to run only on first launch

  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: false,
    arrows: false,
    speed: 5000,
    responsive: [
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
        },
      },
    ],
  };

  // console.log(portfolioData, "portfolioData")

  function getData() {
    db.collection("blog").onSnapshot(function (querySnapshot) {
      setBlogData(
        querySnapshot.docs.map((doc) => ({
          title: doc.data().title,
          author: doc.data().author,
          content: doc.data().content,
          featured_img: doc.data().featured_img,
          metadesc: doc.data().metadesc,
          slug: doc.data().slug,
          keywords: doc.data().keywords,
          date: doc.data().created_at,
        }))
      );
    });
  }


  return (
    <>
      <Seo title={SeoData.title} desc={SeoData.desc} canonical={SeoData.canonical} />
      <Header3 />
      <div className="page-content bg-white" id="top">
        <Slider3 {...BannerData} />
        <AboutUs3  {...AboutUsData} />
        {/* <Services /> */}
        <Service3 {...ServiceSection} />
        {/* <About /> */}

        <AboutUs3  {...AboutUsData2} style={{ flexDirection: "row-reverse", alignItems: "center" }} />
        {/* <Features3 /> */}
        {/* <Projects3 portfolio={portfolioData} desc={"Boundless opportunities of accomplishment"} /> */}
        {/* <PricingT3 /> */}
        {/* <Blog3>
          {blogData.slice(0, 4).map((x, i) => {
            return (
              <div key={x.title + i} className="col-xl-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
                <div className="dlab-blog blog-half m-b30">
                  <div className="dlab-media">
                    <Link href={`/blog/${x.slug}`}><a ><LazyLoadImage src={x.featured_img} alt="" /></a></Link>
                  </div>
                  <div className="dlab-info">
                    <h3 style={{ fontSize: "1.5rem" }} className="dlab-title">
                      <Link href={`/blog/${x.slug}`}><a>{x.title}</a></Link>
                    </h3>
                    <p className="m-b0 blog-desc-style"> <Link href={`/blog/${x.slug}`}><a style={{ textDecoration: "none !important", color: "#505489" }}>{x.metadesc}</a></Link></p>
                  </div>
                </div>
              </div>
            )
          })}
        </Blog3> */}
        {/* <Testimonial3 /> */}
        {/* <Clients3 /> */}
        <Team3 TeamData={TeamData} />
        {/* <GetInTouch /> */}
        <GetInTouch3 contact />
        {/* <Team /> */}
        <Footer3 />

      </div>
    </>
  );
}

export default Index3;



