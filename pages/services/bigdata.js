import Header3 from "../../layout/header-3";
// import Image from "next/image";
import Seo from "../../element/seo";
import Slider3 from "../../element/slider-3";
import ContactForm from "../../element/contact-form";
import AboutUs3 from "../../element/aboutUs-3";
// import GetInTouch3 from "../element/get-in-touch";
import Team3 from "../../element/team-3";
import Blog3 from "../../element/blog-3";
import { useEffect, useState } from "react";
import { db, } from "../../firebase_config";
import Link from "next/link";
import { LazyLoadImage } from "react-lazy-load-image-component";
import FAQ from "../../element/faq";
import Features from "../../element/features";
import FeaturesComponents from "../../element/featuresComponents";
import Footer3 from "../../layout/footer-3";
import Connect_with_us from "../../element/connect_with_us";

function BigData() {
    const [blogData, setBlogData] = useState([]);
    useEffect(() => {
        getData();
    }, []); // blank to run only on first launch

    function getData() {
        db.collection("blog").onSnapshot(function (querySnapshot) {
            setBlogData(
                querySnapshot.docs.map((doc) => ({
                    title: doc.data().title,
                    author: doc.data().author,
                    content: doc.data().content,
                    featured_img: doc.data().featured_img,
                    metadesc: doc.data().metadesc,
                    slug: doc.data().slug,
                    keywords: doc.data().keywords,
                    date: doc.data().created_at,
                }))
            );
        });
    }

    return (
        <>
            <Seo title={SeoData.title} desc={SeoData.desc} canonical={SeoData.canonical} />
            {/* navbar header */}
            <Header3 />

            <div className="page-content bg-white" id="top">
                {/* Hero Section contains data & form */}
                <Slider3 {...BannerData} contact={<ContactForm />} form />

                {/* Automation */}
                {/* <AboutUs3  /> */}

                <Features title='Features' >

                    {
                        _Features.map((feature, id) => {
                            return (
                                <div key={id} className="col-lg-6 mt-2">
                                    <FeaturesComponents  {...feature} />
                                </div>
                            )
                        })
                    }

                </Features>

                {/* Making Considerate Choices */}
                <AboutUs3 bggray="bg-gray" {...ProcessOFWorking} style={{ flexDirection: "row-reverse", alignItems: "center" }} />

                {/* Featured Clients */}
                {/* <AboutUs3  {...AboutUsData} title="Featured Clients" featureClient /> */}

                {/* How it Works */}
                <AboutUs3   {...WhyChooseTechnovature} />

                {/* Meet Our Team */}
                <Team3 TeamData={TeamData} title />

                <Connect_with_us {...CTA}  />


                {/* CTA -- Get in Touch Form Components */}
                {/* <GetInTouch3 test /> */}

                {/* FAQ */}
                <FAQ defaultAccordion={defaultAccordion} />

                {/* Featured Blog Posts */}
                <Blog3  >
                    {blogData.slice(0, 4).map((x, i) => {
                        return (
                            <div key={x.title + i} className="col-xl-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
                                <div className="dlab-blog blog-half m-b30">
                                    <div className="dlab-media">
                                        <Link href={`/blog/${x.slug}`}><a ><LazyLoadImage src={x.featured_img} alt="" /></a></Link>
                                    </div>
                                    <div className="dlab-info">
                                        <h3 style={{ fontSize: "1.5rem" }} className="dlab-title">
                                            <Link href={`/blog/${x.slug}`}><a>{x.title}</a></Link>
                                        </h3>
                                        <p className="m-b0 blog-desc-style"> <Link href={`/blog/${x.slug}`}><a style={{ textDecoration: "none !important", color: "#505489" }}>{x.metadesc}</a></Link></p>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </Blog3>
            </div>
            <Footer3 />
        </>
    )
}


export default BigData;


const defaultAccordion = [
    {
        title: "What is big data?",
        simple: [
            "There is a lot of hype around big data. So what is big data? And more importantly, what can you do with it?",
            "Big data is a term used to describe the large and growing volume of data that organizations are collecting. The volume of data is so large that traditional data management techniques are no longer effective.",
            "Big data can be used to improve decision making, optimize operations, and create new products and services.",
            "There are three key challenges associated with big data:",
            "1. Collecting data from a variety of sources",
            "2. Processing data to extract value",
            "3. Delivering data to decision makers in a timely manner",
            "Organizations are addressing these challenges by implementing big data technologies and solutions.",
            "Big data is a term used to describe the large and growing volume of data that organizations are collecting. The volume of data is so large that traditional data management techniques are no longer effective.",
        ],
        bg: "success",
    },
    {
        title: "How big is big data?",
        text:
            'The term “big data” is used to describe the large and ever-growing volume of data that organizations are collecting, storing and analyzing. The growth of big data is driven by a number of factors, including the proliferation of sensors and devices that are collecting data, the growth of social media and the increasing use of cloud-based services.',
        text1: "The term “big data” is used to describe the large and ever-growing volume of data that organizations are collecting, storing and analyzing. The growth of big data is driven by a number of factors, including the proliferation of sensors and devices that are collecting data, the growth of social media and the increasing use of cloud-based services.",    
        bg: "success",
    },
    {
        title: "Why is big data important?",
        
        text: "An ever-growing deluge of digital data is overwhelming businesses and organizations of all sizes. The volume, variety and velocity of data presents a unique challenge for companies trying to make use of it. The key to unlocking the value of big data is turning it into useful information. That's where big data analytics comes in.",

        text1: "Big data analytics is the process of transforming large amounts of data into meaningful information. It involves using sophisticated software to identify patterns and trends in data, and then using that information to make better decisions.",

        text2: "Big data analytics can help businesses improve their understanding of their customers, their products and their markets. It can help them identify new opportunities, optimize operations and make better decisions.",

        text3: "The benefits of big data analytics are clear, but many companies are still struggling to make use of big data. One of the biggest challenges is finding the right people with the right skills to do the job.",

        bg: "success",
    },
    {
        title: "- Big data analytics: ",
        text: "What is big data analytics? ",
        text1: "Big data analytics is the process of examining large data sets to uncover hidden patterns, unknown correlations, and other useful information. The goal of big data analytics is to gain insights that can help businesses make better decisions and improve their operations.",
        text2: "Big data analytics is a relatively new field, and there are no clear-cut definitions of what it is or what it encompasses. However, the term is generally used to describe the process of analyzing large data sets in order to find trends and patterns.",
        text3: "Big data analytics is often used to improve business operations, but it can also be used for other purposes such as scientific research and crime prevention.",
        text4:"There are several different tools and techniques that can be used for big data analytics, including data mining, predictive modeling, and machine learning.",
        
        bg: "success",
    },
    {
        title: "How is big data analytics used?",
        text: "Big data analytics is used to help organizations make better decisions by understanding and analyzing large and complex data sets. The techniques used in big data analytics can help identify patterns and trends that can help organizations improve their operations, products, and services.",
        text1: "Big data analytics is used to help organizations make better decisions by understanding and analyzing large and complex data sets. The techniques used in big data analytics can help identify patterns and trends that can help organizations improve their operations, products, and services.",
        text2: "The goal of big data analytics is to find meaning in data sets that are too large or complex to be understood by humans. By analyzing data in this way, organizations can gain a better understanding of what is happening in their business and identify opportunities for improvement.",
        text3: "Big data analytics is used in a number of different ways, including:",
        text3UL: [
            "To identify customer trends and preferences",
            "To improve product design and development",
            "To understand market dynamics"
        ],
        
        bg: "success",
    },
    {
        title: "What is big data management?",
        text: "Big data management is the process of managing large data sets, which is becoming increasingly difficult due to the growth of data volume, velocity, and variety. This means that businesses need to find a way to collect, store, and analyze all of this data in order to make informed decisions.",
        text1: "There are a few different ways to manage big data, but the most common is through a data warehouse. A data warehouse is a storage system that is specifically designed to hold large amounts of data. It can be divided into different sections, or tables, so that the data can be easily accessed.",
        
        bg: "success",
    },
    {
        title: "How is big data management used? ",
        text: "Big data management is used to manage large and complex data sets. These data sets can be difficult to manage using traditional database management tools. Big data management tools are designed to manage large data sets and to make it easier to analyze and extract information from these data sets.",
        text1: "The use of big data management tools can help organizations to better understand their customers and to make more informed business decisions. Big data management tools can also help organizations to identify and respond to trends and to better understand how their business is performing.",
        
        bg: "success",
    },
    {
        title: "What are the benefits of big data management?",
        text: "The benefits of Big Data Management are vast and varied. By collecting and managing large amounts of data, organizations can gain insights that would otherwise be unavailable. This can help them make better decisions, improve their operations, and compete more effectively.",
        text1: "Big Data Management can also help organizations find and fix problems more quickly. By analyzing large amounts of data, they can identify patterns and correlations that would be difficult to spot with smaller data sets. This can help them troubleshoot issues and improve their products and services.",
        text2: "Big Data Management can also help organizations improve their customer service. By analyzing customer data, they can identify trends and preferences. This can help them create a more personalized experience for their customers and improve customer satisfaction.",
        text3: "The benefits of Big Data Management are vast and varied. By collecting and managing large amounts of data, organizations can gain insights that would otherwise be unavailable. This can help them make better decisions, improve their operations, and compete more effectively.",
        text4: "Big Data Management can also help organizations find and fix problems more quickly. By analyzing large amounts of data, they can identify patterns and correlations that would be difficult to spot with smaller data sets. This can help them troubleshoot issues and improve their products and services.",
        text5: "Big Data Management can also help organizations improve their customer service. By analyzing customer data, they can identify trends and preferences. This can help them create a more personalized experience for their customers and improve customer satisfaction.This can help them make better decisions.",
        
        bg: "success",
    },
];

// Artificial Intelligence Page
const SeoData = {
    title: "Big Data | Technovature Software Solutions Pvt. Ltd.",
    desc: "High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation",
    canonical: "https://www.technovature.com/services/bigdata"
}
const TeamData = [
    { name: "Kalyani Jallapuram", img: "../images/team/small/kalyani.jpeg", position: "Director, Operations", dealy: "0.1s" },
    { name: "Srikanth Jallapuram", img: "../images/team/small/shrikanth.jpeg", position: "Founder Director & CEO", delay: "0.2s" },
]
const BannerData = {
    subtitle: "Since the world has turned towards Data, Technovature has made it a point to deliver you the ease of technology and innovation to make the most of your Business.",
    title: "Enter the Data-oriented business world for the emergence of success",
    desc: "Businesses use big data to enhance operations, deliver better customer service, develop individualized marketing campaigns, and carry out other tasks that can eventually boost sales and profits. Because they can act more quickly and with more knowledge, businesses that use it efficiently may have a competitive edge over those that don't.",
    desc1: "Big data, for instance, offers insightful information about consumers that businesses can utilize to improve their marketing, advertising, and promotions and boost customer conversion rates and engagement. Businesses could become more responsive to client demands and needs by analyzing historical and real-time data to gauge the changing preferences of customers or corporate purchasers.",
    desc2: null,
    link: "/#contact-now"
}

const _Features = [
    {
        title: null,
        desc1: "It implies a vast amount of information that keeps exponentially increasing throughout time.",
    },
    {
        title: null,
        desc1: "Because it is so large, it cannot be handled or examined using standard data processing methods.",
    },
    {
        title: null,
        desc1: "Data mining, storage, analysis, sharing, and visualization are all included.",
    },
    {
        title: null,
        desc1: "The phrase refers to anything related to processing and analyzing data, including data, data frameworks, tools, and processes.",
    }
]

const ProcessOFWorking = {
    img: "../newImg/about/bigdata-1.svg",
    title: "Process of working ",
    // desc1:"Unfurling businesses’ digital aptitudes.",
    // desc2: <>We are “Digital Transforming Facilitators.” Bridging the gap of the digital needs of a business and accurate, custom, high-quality solutions, we offer our clients an array of tech-based services. At Technovature, we believe in scalability and sustainability, and hence our cutting-edge tailored solutions ensure grooming business “Future Ready.”</>,
    // desc2: "Is your organization geared up for digital transformation? Technovature empowers your business with cutting-edge technologies customized to accomplish your business goal.",
    // counterData: [
    //   { delay: "0.2s", title: "Years of experience",sign:"+", count: "5" },
    //   { delay: "0.4s", title: <>Projects<br/> handled</>,sign:"+", count: "100" },
    //   { delay: "0.6s", title: "Satisfied customers",sign:"+", count: "98" },
    // ],
    listData: [
        { content: "Our team will read and understand your requirement" },
        { content: "We will analyze and project a plan for you" },
        { content: "Displaying it in front of you for an approval " },
        { content: "Execution and development will start" },
    ]
}

const WhyChooseTechnovature = {
    img: "../newImg/about/bigdata-2.svg",
    title: "Why choose Technovature?",
    desc1: "At Technovature, we think that a business should not only survive but also prosper in the impending Golden Digital Future period. To reach that purpose, we cooperate and work with the Business in unprecedented ways.",
    desc2: 'Cloud computing, in its most basic form, is the transmission of computer services such as servers, memory, databases, networking, software, analytics, and intelligence via the Internet ("the cloud") in order to deliver faster innovation, more flexible capabilities, and economies of scale.',
}

const CTA = {
    img: "",
    title: "",
    desc1: "The secret to unveiling the future of the company is digital transformation. We have a robust culture of creativity, innovation and discovery at Technovature. We are continually seeking new approaches and abilities and embracing cutting-edge technology that might unlock the possibilities of a digital world.",
    desc2: "Let Technovature Technologize and Transform your enterprise.",
    btn: "Connect with us today!"
}
