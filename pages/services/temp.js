import React from 'react';
import Projects3 from '../../element/Projects-3';
import Seo from '../../element/seo';
import Footer3 from '../../layout/footer-3';
import Header3 from '../../layout/header-3';

const Temp = () => {
  return (
    <>
      <Seo title={SeoData.title} desc={SeoData.desc} canonical={SeoData.canonical} />
      <Header3 />
      {/* <!-- Banner  --> */}
      {/* <div
          className="dlab-bnr-inr style-1 bg-primary"
          style={{
            backgroundImage: "url(../images/banner/bnr2.png), var(--gradient-sec)",
            backgroundSize: "cover, 200%",
          }}
        >
          <div className="container">
            <div className="dlab-bnr-inr-entry">
              <h1>Services</h1>
            </div>
          </div>
        </div> */}
        {/* <!-- Banner End --> */}
      <Projects3 Heading={Heading} Data={Data} title="Learn. Practice. Earn. Have Fun!" />

      <Footer3 />
    </>
  )
}

export default Temp;

const SeoData = {
  title: "Services | Technovature Software Solutions Pvt. Ltd.",
  desc: "High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation",
  canonical: "https://www.technovature.com/services/temp"
}

const Data = [
  {
    category: ["blockchain"],
    img: "/newImg/services/self/blockchain.png",
    title: "Blockchain",
    desc1: "Blockchain technology's fast advancement shows no signs of halting. Many seemingly implausible things have turned out to be incorrect throughout the last few decades, such as exorbitant transaction costs, double spending, net fraud, recovering lost data, etc. ",
    desc2: "All of this, however, may now be prevented because of Blockchain Technology.",
    feature: [
      "It is a public digital ledger that is immutable, which means that once a transaction is recorded, this can be changed.",
      "Because of the encryption mechanism, Blockchain is always secure.",
      "Because the ledger is automatically updated, the transactions are executed instantly and transparently.",
      "There is no need for an intermediary charge because it is decentralized.",
      "Participants confirm and certify the authenticity of the transaction."
    ],
    changelayout: 'changelayout',
    link: "/services/blockchain",
    ID: "removeP",
    Target: 'blockchain'
  },
  {
    category: ["cloud"],
    img: "/newImg/services/self/cloud_computing.png",
    title: "Cloud Computing",
    desc1: "Essentially, suppose you use an internet service to send an email, edit documents, view movies or TV, listen to music, play games, or save images and other information. In that case, cloud computing enables it all behind the scenes. Even though the first cloud computing services are just a decade old, a wide range of businesses, including huge corporations, nonprofits, and government institutions, are already implementing the technology.",
    desc2: null,
    feature: [
      "Cost efficient",
      "High speed",
      "Greater productivity",
      "Available on global platforms",
      "Reliable and better scope of performance",
      "Improved security and safety"
    ],
    link: "/services/cloud",
    ID: "removeP",
    Target: 'cloud_computing'
  },
  {
    category: ["bigdata"],
    img: "/newImg/services/self/bigdata.png",
    title: "Big Data",
    desc1: "Big data is characterized as data that is more diverse, produced in larger quantities, and moves more quickly. The three Vs are another name for this.",
    desc2: "Big data, for example, gives important consumer insights that businesses can utilize to improve their marketing, advertising, and promotions in order to enhance customer engagement and conversion rates. Historical and real-time data may be evaluated to assess changing consumer or corporate buyer preferences, allowing organizations to respond more to client demands and needs.",
    feature: [
      "Data Quantity is also known as volume",
      "The speed with which data is processed is referred to as velocity.",
      "The value of data refers to the advantages that your organization obtains from it.",
      'The term "variety" refers to the various sorts of big data. It is one of the most serious concerns confronting the big data business since it has a negative impact on performance.',
      "The correctness of your data is referred to as its veracity. It is one of the most crucial Big Data features since poor veracity may significantly impact the correctness of your findings.",
      "The data must be valid and relevant in order to be utilized for the original purpose.",
      "Big data volatility is unpredictable. The data you collected from a resource a day ago may differ from what you discovered today.",
      "Visualization displays large data insights through pictorial imagery such as graphs and charts."
    ],
    link: "/services/bigdata",
    ID: "removeP",
    changelayout: 'changelayout',
    Target: 'bigdata'

  },
  {
    category: ["iot"],
    img: "/newImg/services/self/iot.png",
    title: "Internet of Things",
    desc1: "IoT has emerged as one of the key inventions of the twenty-first century in recent years. Now that ubiquitous goods like kitchen appliances, cars, thermostats, and baby monitors can connect to the Internet via embedded devices, seamless communication between people, processes, and objects is possible. Experts predict that the number of linked IoT devices will increase to 10 billion by 2020 and 22 billion by 2025, from more than 7 billion. Oracle has a device partner network.",
    desc2: null,
    feature: [
      "Access to low-cost, low-power sensor technologies. The availability of inexpensive, reliable sensors has made IoT technology more accessible to manufacturers.",
      'Connectivity. Thanks to the abundance of internet network protocols, it is now simple to connect sensors to the cloud and other "things" for quick data transfer.',
      "Web-based cloud computing platforms Enterprises and consumers may now get the infrastructure they need to scale up without having to manage it, all thanks to the growing accessibility of cloud platforms.",
      "Analytics and machine learning Businesses may obtain insights quicker and more simply with developments in machine learning and analytics, as well as access to diverse and large volumes of data stored on the cloud.",
      "The growth of these linked innovations continues to push the frontiers of IoT, and IoT data also feeds these technologies.",
      "Artificial conversational intelligence (AI). Natural-language processing (NLP) has been extended to IoT devices (such as digital personal assistants Alexa, Cortana, and Siri) through advances in neural networks, making them attractive, inexpensive, and feasible for home usage"
    ],
    link: "/services/iot",
    ID: "removeP",
    Target: 'iot'
  },
  {
    category: ["ai"],
    img: "/newImg/services/self/ai.png",
    title: "Artificial intelligence",
    desc1: "To explain to you, artificial intelligence is an area of work that includes large datasets and computer science to solve endless problems. It also includes the subfields of machine learning and deep learning commonly referenced in the context of artificial intelligence. AI algorithms develop expert systems that make forecasts or classifications based upon input data. It has been created to improve human skills and add value to their contribution. As a result, it is an extremely important commercial asset.",
    desc2: null,
    feature: [
      "The availability of heavy data and cloud computing for training",
      'Affordability and high performance',
      "Competitive advantage with applied AI",
    ],
    link: "/services/ai",
    ID: "removeP",
    changelayout: 'changelayout',
    Target: 'autificial_intelligence'


  },
  {
    category: ["webdev"],
    img: "/newImg/services/self/web_development.png",
    title: "Web development",
    desc1: 'Web development is closely related to developing the features and functionality of websites and apps, sometimes known as "web design," even if the term "web development" is generally reserved for the actual production and programming of websites and applications. If you want to enhance your online reputation, you need to consider a number of things, especially in web development.',
    desc2: null,
    feature: [
      "Navigation made easier",
      'Attractive content and visual elements',
      "Maintained consistency",
      "Association and winning the SEO aspect",
      "Potential hike in sales of your business",
      "Attracting the clients to the max",
      "Setting an impression for your customers"
    ],
    link: "/services/fintech",
    ID: "removeP",
    Target: 'web_development'

  },
]



const Heading = [{
  btn_s: "blockchain",
  btn_l: "Blockchain",
},
{
  btn_s: "cloud",
  btn_l: "Cloud Computing",
},
{
  btn_s: "bigdata",
  btn_l: "Big Data",
},
{
  btn_s: "iot",
  btn_l: "Internet of Things",
},
{
  btn_s: "ai",
  btn_l: "Artificial intelligence",
},
{
  btn_s: "webdev",
  btn_l: "Web development",
}
]
