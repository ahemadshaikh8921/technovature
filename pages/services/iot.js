import Header3 from "../../layout/header-3";
// import Image from "next/image";
import Seo from "../../element/seo";
import Slider3 from "../../element/slider-3";
import ContactForm from "../../element/contact-form";
import AboutUs3 from "../../element/aboutUs-3";
// import GetInTouch3 from "../element/get-in-touch";
import Team3 from "../../element/team-3";
import Blog3 from "../../element/blog-3";
import { useEffect, useState } from "react";
import { db, } from "../../firebase_config";
import Link from "next/link";
import { LazyLoadImage } from "react-lazy-load-image-component";
import FAQ from "../../element/faq";
import Features from "../../element/features";
import FeaturesComponents from "../../element/featuresComponents";
import Footer3 from "../../layout/footer-3";
import Connect_with_us from "../../element/connect_with_us";

function IOT() {
  const [blogData, setBlogData] = useState([]);
  useEffect(() => {
    getData();
  }, []); // blank to run only on first launch

  function getData() {
    db.collection("blog").onSnapshot(function (querySnapshot) {
      setBlogData(
        querySnapshot.docs.map((doc) => ({
          title: doc.data().title,
          author: doc.data().author,
          content: doc.data().content,
          featured_img: doc.data().featured_img,
          metadesc: doc.data().metadesc,
          slug: doc.data().slug,
          keywords: doc.data().keywords,
          date: doc.data().created_at,
        }))
      );
    });
  }

  return (
    <>
      <Seo title={SeoData.title} desc={SeoData.desc} canonical={SeoData.canonical} />
      {/* navbar header */}
      <Header3 />

      <div className="page-content bg-white" id="top">
        {/* Hero Section contains data & form */}
        <Slider3 {...BannerData} contact={<ContactForm />} form />

        {/* Automation */}
        {/* <AboutUs3  /> */}

        <Features title='Features' >

          {
            _Features.map((feature, id) => {
              return (
                <div key={id} className="col-lg-6 mt-2">
                  <FeaturesComponents  {...feature} />
                </div>
              )
            })
          }

        </Features>

        {/* Making Considerate Choices */}
        <AboutUs3 bggray="bg-gray" {...ProcessOFWorking} style={{ flexDirection: "row-reverse", alignItems: "center" }} />

        {/* Featured Clients */}
        {/* <AboutUs3  {...AboutUsData} title="Featured Clients" featureClient /> */}

        {/* How it Works */}
        <AboutUs3  {...WhyChooseTechnovature} />

        {/* Meet Our Team */}
        <Team3 TeamData={TeamData} title />

        <Connect_with_us {...CTA} />


        {/* CTA -- Get in Touch Form Components */}
        {/* <GetInTouch3 test /> */}

        {/* FAQ */}
        <FAQ defaultAccordion={defaultAccordion} />

        {/* Featured Blog Posts */}
        <Blog3  >
          {blogData.slice(0, 4).map((x, i) => {
            return (
              <div key={x.title + i} className="col-xl-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
                <div className="dlab-blog blog-half m-b30">
                  <div className="dlab-media">
                    <Link href={`/blog/${x.slug}`}><a ><LazyLoadImage src={x.featured_img} alt="" /></a></Link>
                  </div>
                  <div className="dlab-info">
                    <h3 style={{ fontSize: "1.5rem" }} className="dlab-title">
                      <Link href={`/blog/${x.slug}`}><a>{x.title}</a></Link>
                    </h3>
                    <p className="m-b0 blog-desc-style"> <Link href={`/blog/${x.slug}`}><a style={{ textDecoration: "none !important", color: "#505489" }}>{x.metadesc}</a></Link></p>
                  </div>
                </div>
              </div>
            )
          })}
        </Blog3>
      </div>
      <Footer3 />
    </>
  )
}


export default IOT;

const defaultAccordion = [
  {
    title: "What is the Internet of Things?",
    text:
      "The term 'Internet of Things' has been around since the 1990s, and it refers to the idea that someday, all devices will be connected to the internet and will be able to communicate with each other. We're getting closer and closer to that reality every day, as more and more devices are able to connect to the internet.",
    text2: "The Internet of Things has a lot of potential applications, and it could be used for things like monitoring patients' health, tracking shipments, or controlling energy consumption. It could also be used for things like monitoring traffic or preventing crime.",
    text3: "There are a lot of different devices that can be part of the Internet of Things, including things like cars, appliances, and medical devices. There are also a lot of different ways to connect these devices to the internet, including things like Bluetooth, Wi-Fi, and RFID.",

    bg: "success",
  },
  {
    title: "How does the Internet of Things work?",
    text:
      "The Internet of Things (IoT) is the network of physical objects, devices, vehicles, and buildings that are embedded with electronics, software, sensors, and network connectivity that enables these objects to collect and exchange data. The IoT has the potential to enable new services and capabilities, and to improve the efficiency, safety, and sustainability of our physical world.",

    text1: "The IoT is built on three foundational technologies:",

    text1UL: [
      "Connectivity: Devices must be connected to the network in order to exchange data. Connectivity can be provided through a variety of means, including Bluetooth, Wi-Fi, 3G/4G, and LPWA technologies.",

      "Embedded Systems: Devices must have the ability to run embedded software and sensors. This software enables devices to interact with their environment, collect and process data, and respond to commands.",

      "Big Data and Analytics: Devices must be able to collect, process, and analyze data in order to make informed",
    ],

    bg: "success",
  },
  {
    title: "What are the benefits of the Internet of Things? ",
    text:
      "The Internet of Things has already begun to revolutionize the way we interact with the world around us. By connecting everyday objects to the internet, we are able to access, monitor, and control them in ways never before possible. This has led to a number of benefits for both individuals and businesses.",

    text1: "Some of the benefits of the Internet of Things include:",

    text1UL: [
      "Increased efficiency and productivity. The IoT can help you track and manage your resources more efficiently, saving time and money.",

      "Greater convenience. With the IoT, you can control your devices from anywhere in the world.",

      "Enhanced security. The IoT can help you protect your property and personal information from theft and other threats.",

      "Improved communication. The IoT can help you connect with your friends, family, and colleagues in ways never before possible.",

      "Increased creativity and innovation. The IoT has the potential to inspire new and innovative ways of thinking about the worl",
    ],

    bg: "success",
  },
  {
    title: "What are the challenges of the Internet of Things?",
    text:
        "The Internet of Things (IoT) is a term for the ever-growing network of physical objects that are connected to the Internet. These objects can include anything from home appliances to cars to industrial equipment. The IoT presents a number of challenges for businesses and individuals.",

    text1: "One of the biggest challenges of the IoT is security. With so many objects connected to the Internet, it is difficult to ensure that all of them are secure. Hackers can exploit vulnerabilities in the IoT to gain access to sensitive data or even take control of devices.",

    text2: "Another challenge of the IoT is managing all of the data that is generated by these devices. The IoT generates a huge volume of data, and businesses need to find ways to manage and analyze this data in order to gain insights into their operations.",

    text3: "The IoT also raises privacy concerns. As more and more devices are connected to the Internet, it becomes increasingly difficult to keep track of who is collecting data and what they are doing with",

    bg: "success",
},
{
  title: "How can the Internet of Things be used?",
  text:
    "The internet of things has the potential to change the way we interact with the world around us. With the internet of things, objects can be connected to the internet and to each other. This allows for objects to communicate with each other and to be controlled remotely. The internet of things has a variety of applications, including the following:",

  text1UL: [
    "Home automation: Home automation allows you to control your home’s appliances and devices remotely. With home automation, you can turn on your lights, turn off your TV, and even adjust your thermostat from anywhere in the world.",

    "Smart cities: Smart cities are cities that are equipped with sensors and devices that allow for the collection of data. This data can be used to improve the efficiency of city services, such as transportation and energy consumption.",

    "Health care: The internet of things can be used to improve health care. For example, sensors can be used to monitor patients’ health etc.",

  ],

  bg: "success",
},
{
  title: "What industries will the Internet of Things impact? ",
  text:
    "The Internet of Things (IoT) is a term that is used to describe the interconnectedness of physical objects and devices that are connected to the internet. The IoT has the potential to impact a wide range of industries, including healthcare, transportation, manufacturing, and agriculture.",

  text1: "The healthcare industry is one of the sectors that is expected to be impacted the most by the IoT. The IoT can be used to monitor the health of patients and to track the progress of diseases. For example, sensors can be used to track the heart rate and blood pressure of patients. The IoT can also be used to improve the efficiency of healthcare providers. For example, hospitals can use sensors to track the use of medical equipment and to optimize the use of resources.",

  bg: "success",
},
{
  title: "What is the future of the Internet of Things?",
  text:
    "The future of the Internet of Things is shrouded in potential but fraught with uncertainty. The IoT is still in its infancy, and there are many unanswered questions about its future.",

  text1: "One key question is how the IoT will evolve. Some observers believe that the IoT will evolve into a more centralized, corporate-controlled network. Others believe that the IoT will be more decentralized, with a greater emphasis on individual users and small businesses.",

  text2: "Another key question is how the IoT will be regulated. There are concerns that the IoT could be used to invade people's privacy or to interfere with critical infrastructure. There are also concerns that the IoT could be used to spread disinformation or to manipulate public opinion.",

  text3: "These are just a few of the questions that will need to be answered in order to determine the future of the IoT. There are many other questions, including the following:",

  text3UL: [
    "How will the IoT be secured?",

    "How will it be financed?",

    "What open standards will it follow?",

],

  bg: "success",
},

];



// Artificial Intelligence Page
const SeoData = {
  title: "Internet of Things | Technovature Software Solutions Pvt. Ltd.",
  desc: "High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation",
  canonical: "https://www.technovature.com/services/iot"
}
const TeamData = [
  { name: "Kalyani Jallapuram", img: "../images/team/small/kalyani.jpeg", position: "Director, Operations", dealy: "0.1s" },
  { name: "Srikanth Jallapuram", img: "../images/team/small/shrikanth.jpeg", position: "Founder Director & CEO", delay: "0.2s" },
]
const BannerData = {
  subtitle: "Measure the success of your Business with the help of technology and innovation for a smoother ride of entrepreneurship",
  title: "Experience the innovation and Internet of things like never before",
  desc: "Today, millions of various gadgets transfer data across the Internet with no human intervention. IoT Analytics predicts that by 2025, there will be more than 27 billion IoT connections in its State of IoT — Summer 2021 study.",
  desc1: "Everything may be permanently connected to the Internet and become a part of the IoT Architecture, from refrigerators to air purifiers to wall outlets.",
  desc2: "The fact that compact and practical chips are getting less expensive to create and deliver is a significant factor in this exponential development. It is also a result of concrete actions in creating wireless technologies suitable for the Internet of Things, including the Z-Wave and Zigbee protocols. Because of this, IoT can benefit us in unique ways and continue to expand.",
  link: "/#contact-now"
}

const _Features = [
  {
    title: "Automation and Control",
    desc1: "The standard smart bulb is one of the most significant and straightforward instances of control and automation through an IoT device, even though some may dismiss it as nothing but a gimmick. The popularity of smart light bulbs has skyrocketed recently for a good reason.",
  },
  {
    title: "Real-time Information Access",
    desc1: "The constant flow of information is one of the main benefits of the Internet of things. Data may be sent between internet-connected devices at the speed of light, resulting in fewer delays and less chance of misunderstandings.",
  },
  {
    title: "Emerging Companies",
    desc1: "IoT is no exception to the rule that the advent of new technology always results in an explosion of new business prospects. Even though intelligent gadgets have nearly thoroughly permeated society, we are still years, if not decades, from global usage.",
  }
]

const ProcessOFWorking = {
  img: "../newImg/about/iot-4.png",
  title: "Process of working ",
  // desc1:"Unfurling businesses’ digital aptitudes.",
  // desc2: <>We are “Digital Transforming Facilitators.” Bridging the gap of the digital needs of a business and accurate, custom, high-quality solutions, we offer our clients an array of tech-based services. At Technovature, we believe in scalability and sustainability, and hence our cutting-edge tailored solutions ensure grooming business “Future Ready.”</>,
  // desc2: "Is your organization geared up for digital transformation? Technovature empowers your business with cutting-edge technologies customized to accomplish your business goal.",
  // counterData: [
  //   { delay: "0.2s", title: "Years of experience",sign:"+", count: "5" },
  //   { delay: "0.4s", title: <>Projects<br/> handled</>,sign:"+", count: "100" },
  //   { delay: "0.6s", title: "Satisfied customers",sign:"+", count: "98" },
  // ],
  listData: [
    { content: "Our team will read and understand your requirement" },
    { content: "We will analyze and project a plan for you" },
    { content: "Displaying it in front of you for an approval " },
    { content: "Execution and development will start" },
  ]
}

const WhyChooseTechnovature = {
  img: "../newImg/about/iot-3.svg",
  title: "Why choose Technovature?",
  desc1: "At Technovature, we think a business should survive and prosper in the impending Golden Digital Future period. To reach that purpose, we cooperate and work with the Business in unprecedented ways.",
  desc2: 'In its basic form, cloud computing is the transmission of computer services such as servers, memory, databases, networking, software, analytics, and intelligence via the Internet ("the cloud") to deliver faster innovation, more flexible capabilities, and economies of scale.',
}

const CTA = {
  img: "",
  title: "",
  desc1: "The secret to unveiling the future of the company is digital transformation. We have a robust culture of creativity, innovation and discovery at Technovature. We are continually seeking new approaches and abilities and embracing cutting-edge technology that might unlock the possibilities of a digital world.",
  desc2: "Let Technovature Technologize and Transform your enterprise. ",
  btn: "Connect with us today! "
}
