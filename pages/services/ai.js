import Header3 from "../../layout/header-3";
// import Image from "next/image";
import Seo from "../../element/seo";
import Slider3 from "../../element/slider-3";
import ContactForm from "../../element/contact-form";
import AboutUs3 from "../../element/aboutUs-3";
// import GetInTouch3 from "../element/get-in-touch";
import Team3 from "../../element/team-3";
import Blog3 from "../../element/blog-3";
import { useEffect, useState } from "react";
import { db, } from "../../firebase_config";
import Link from "next/link";
import { LazyLoadImage } from "react-lazy-load-image-component";
import FAQ from "../../element/faq";
import Features from "../../element/features";
import FeaturesComponents from "../../element/featuresComponents";
import Footer3 from "../../layout/footer-3";
import Connect_with_us from "../../element/connect_with_us";

function AI() {
  const [blogData, setBlogData] = useState([]);
  useEffect(() => {
    getData();
  }, []); // blank to run only on first launch

  function getData() {
    db.collection("blog").onSnapshot(function (querySnapshot) {
      setBlogData(
        querySnapshot.docs.map((doc) => ({
          title: doc.data().title,
          author: doc.data().author,
          content: doc.data().content,
          featured_img: doc.data().featured_img,
          metadesc: doc.data().metadesc,
          slug: doc.data().slug,
          keywords: doc.data().keywords,
          date: doc.data().created_at,
        }))
      );
    });
  }

  return (
    <>
      <Seo title={SeoData.title} desc={SeoData.desc} canonical={SeoData.canonical} />
      {/* navbar header */}
      <Header3 />

      <div className="page-content bg-white" id="top">
        {/* Hero Section contains data & form */}
        <Slider3 {...BannerData} contact={<ContactForm />} form />

        {/* Automation */}
        {/* <AboutUs3  /> */}

        <Features title='Features' >

          {
            _Features.map((feature, id) => {
              return (
                <div key={id} className="col-lg-6 mt-2">
                  <FeaturesComponents  {...feature} />
                </div>
              )
            })
          }

        </Features>

        {/* process of working components*/}
        <AboutUs3 bggray="bg-gray" {...ProcessOFWorking} style={{ flexDirection: "row-reverse", alignItems: "center" }} />

        {/* Featured Clients */}
        {/* <AboutUs3  {...AboutUsData} title="Featured Clients" featureClient /> */}

        {/* why choose tech components */}
        <AboutUs3   {...WhyChooseTechnovature} />

        {/* Meet Our Team */}
        <Team3 TeamData={TeamData} title />

        {/* <AboutUs3 {...CTA} style={{ flexDirection: "row-reverse", alignItems: "center" }} /> */}
        <Connect_with_us {...CTA} />

        {/* FAQ */}
        <FAQ defaultAccordion={defaultAccordion} />

        {/* Featured Blog Posts */}
        <Blog3  >
          {blogData.slice(0, 4).map((x, i) => {
            return (
              <div key={x.title + i} className="col-xl-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
                <div className="dlab-blog blog-half m-b30">
                  <div className="dlab-media">
                    <Link href={`/blog/${x.slug}`}><a ><LazyLoadImage src={x.featured_img} alt="" /></a></Link>
                  </div>
                  <div className="dlab-info">
                    <h3 style={{ fontSize: "1.5rem" }} className="dlab-title">
                      <Link href={`/blog/${x.slug}`}><a>{x.title}</a></Link>
                    </h3>
                    <p className="m-b0 blog-desc-style"> <Link href={`/blog/${x.slug}`}><a style={{ textDecoration: "none !important", color: "#505489" }}>{x.metadesc}</a></Link></p>
                  </div>
                </div>
              </div>
            )
          })}
        </Blog3>
      </div>
      <Footer3 />
    </>
  )
}


export default AI;


const defaultAccordion = [
  {
    title: "What is AI? How does AI work? ",
    text0UL: [
      "AI is a field of computer science and engineering focused on the creation of intelligent agents, which are systems that can reason, learn, and act autonomously. AI has been around for decades, but has only recently become widely available to the public. This is due in part to the increased availability of data and computing power, as well as the development of better algorithms and AI frameworks.",
      "There are many different types of AI, but most AI systems share some common components. These components include a knowledge base, a reasoning engine, and a learning algorithm. The knowledge base stores information about the world, including facts, rules, and relations between objects. The reasoning engine uses the information in the knowledge base to reason about the world and figure out how to achieve goals. The learning algorithm uses experience to learn how to achieve goals."
    ],
    bg: "success",
  },
  {
    title: "Types of AI:",

    text0UL: ["Machine Learning"],

    text0UL_UL: ["Machine Learning is a method for data mining and predictive modeling that uses algorithms to analyze data and learn from it. The algorithms can be used to make predictions about future events, or to identify patterns in past data. Machine Learning is a subset of Artificial Intelligence, and is often used for tasks such as predictive modeling, natural language processing and image recognition."],

    text1UL: ["Natural Language Processing "],

    text1UL_UL: ["Natural language processing (NLP) is a field of artificial intelligence (AI) that deals with the understanding of human language. It involves the development of computer programs that can read and understand text, as well as generate text that sounds like it was written by a human.", "One of the primary goals of NLP is to create computers that can understand natural language as it is spoken by humans. This would allow for the development of chatbots and other types of AI-powered assistants that could understand what we are saying and respond in a way that makes sense."],


    bg: "success",
  },
  {
    title: "Applications of AI",

    text0UL: ["Robotics"],

    text0UL_UL: ["Robotics refers to the use of machines to carry out specific tasks, typically involving movement and control. Robotics technology has become increasingly prevalent in manufacturing and other industrial applications, and is now being explored for use in a wider range of tasks, including domestic and personal service"],

    text1UL: ["Autonomous Vehicles "],

    text1UL_UL: ["Auto-pilot technology has been around in one form or another since the early days of aviation. However, the application of this technology to automobiles is a more recent development. In its simplest form, autonomous driving technology allows a car to stay in its lane and maintain a set speed without driver input. More sophisticated systems can navigate through intersections and even change lanes.", "The application of autonomous driving technology to automobiles is a more recent development.", "In its simplest form, autonomous driving technology allows a car to stay in its lane and maintain a set speed without driver input. More sophisticated systems can navigate through"],


    bg: "success",
  },
  {
    title: "Limitations of AI: Ethics and Privacy ",
    text0UL: [
      "There are many concerns that people have about artificial intelligence (AI), and rightly so. After all, AI will soon be able to do things that were once thought to be the sole domain of human beings, such as driving cars, diagnosing diseases, and writing articles. With so much at stake, it is important to address the concerns that people have about AI, including its potential impact on ethics and privacy.",
      "One of the main concerns about AI is that it could have a negative impact on ethics. For example, there is a fear that AI could be used to create weapons that can identify and target specific people. AI could also be used to manipulate people’s emotions or to influence their decisions. In addition, there is a concern that AI will have a negative impact on privacy. For example, AI could be used to track people’s movements or to collect their personal data."
    ],
    bg: "success",
  },

  {
    title: "Future of AI: How will AI evolve?",
    text0UL: [
        "The future of AI is difficult to predict, as it depends on a number of factors, including the rate of technological advancement, the availability of data, and the degree of human-machine collaboration. However, we can make some educated guesses about how AI will evolve.",
        "First, AI will become more powerful and more efficient. It will be able to handle more complex tasks and analyze more data more quickly. This will allow AI to become more specialized, and it will be able to tackle more complicated problems.",
        "Second, AI will become more accessible. The cost of developing and using AI will drop, making it more affordable for businesses and individuals. This will increase the number of applications for AI and the amount of data that is processed.",
        "Third, AI will become more collaborative. Humans and machines will work together more closely to achieve common goals. This will allow humans to take advantage of AI’s strengths."
    ],
    bg: "success",
},

];



// Artificial Intelligence Page
const SeoData = {
  title: "Artificial Intelligence | Technovature Software Solutions Pvt. Ltd.",
  desc: "High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation",
  canonical: "https://www.technovature.com/services/ai"
}
const TeamData = [
  { name: "Kalyani Jallapuram", img: "../images/team/small/kalyani.jpeg", position: "Director, Operations", dealy: "0.1s" },
  { name: "Srikanth Jallapuram", img: "../images/team/small/shrikanth.jpeg", position: "Founder Director & CEO", delay: "0.2s" },
]
const BannerData = {
  subtitle: null,
  title: "Embrace the future of technology with Artificial Intelligence",
  desc: "AI is the collective name for a number of technologies that enable robots to detect, learn, comprehend, and act in ways that complement human talents. Large-scale information that can be learned from and handled by AI technology can successfully improve and revolutionize operations in a variety of industries. AI technology can foresee demands and make deft, pertinent judgments after a time of learning and comprehension.",
  desc1: "Although the adoption of AI in modern culture is recent, the idea itself is not. Despite the fact that artificial intelligence (AI) as we know it today was first studied in 1956, significant progress toward developing an AI system and making it a technological reality needed decades of work.",
  desc2: "In Business, artificial intelligence is applied in many different contexts. In reality, most of us interact with AI frequently in one way or another. Almost every business action across all industries is already being disrupted by artificial intelligence, from the mundane to the incredible. As AI technologies proliferate, they are increasingly required to maintain a competitive edge.",
  link: "/#contact-now"
}

const _Features = [
  {
    title: "Automation",
    desc1: "One of the most frequently mentioned advantages of AI technology is automation, which has had a big influence on the communication, transportation, consumer goods, and service sectors.",
  },
  {
    title: "Making Considerate Choices",
    desc1: "Artificial intelligence has long been used to make wise business decisions. AI technology can coordinate data distribution, evaluate trends, provide data consistency, generate forecasts, and quantify uncertainties to help businesses make the best decisions possible.",
  },
  {
    title: "Improved Customer Experience",
    desc1: "Using AI-powered solutions, businesses may respond to customer complaints and inquiries promptly and efficiently.",
  },
  {
    title: "Properly Disposing of Challenges",
    desc1: "AI technology has advanced from basic Machine Learning to complex Deep Learning models, allowing it to solve challenging issues.",
  }
]

const ProcessOFWorking = {
  img: "../newImg/about/ai-about3.svg",
  title: "Process of working ",
  // desc1:"Unfurling businesses’ digital aptitudes.",
  // desc2: <>We are “Digital Transforming Facilitators.” Bridging the gap of the digital needs of a business and accurate, custom, high-quality solutions, we offer our clients an array of tech-based services. At Technovature, we believe in scalability and sustainability, and hence our cutting-edge tailored solutions ensure grooming business “Future Ready.”</>,
  // desc2: "Is your organization geared up for digital transformation? Technovature empowers your business with cutting-edge technologies customized to accomplish your business goal.",
  // counterData: [
  //   { delay: "0.2s", title: "Years of experience",sign:"+", count: "5" },
  //   { delay: "0.4s", title: <>Projects<br/> handled</>,sign:"+", count: "100" },
  //   { delay: "0.6s", title: "Satisfied customers",sign:"+", count: "98" },
  // ],
  listData: [
    { content: "Our team will read and understand your requirement" },
    { content: "We will analyze and project a plan for you" },
    { content: "Displaying it in front of you for an approval" },
    { content: "Great team of highly experienced professionals" },
    { content: "Execution and development will start" },
  ]
}

const WhyChooseTechnovature = {
  img: "../newImg/about/ai-about4.webp",
  title: "Why choose Technovature?",
  // desc1: "At Technovature, we think that a business should not only survive but also prosper in the impending Golden Digital Future period. To reach that purpose, we cooperate and work with the Business in unprecedented ways.",
  // desc2: 'Cloud computing, in its most basic form, is the transmission of computer services such as servers, memory, databases, networking, software, analytics, and intelligence via the Internet ("the cloud") in order to deliver faster innovation, more flexible capabilities, and economies of scale.',
  extraImages: [
    {
      img: "../images/Highly_experienced_team/HET_AI.png",
      title: "Highly experienced team"
    },
    {
      img: "../images/HITAR/HITAR_1.png",
      title: "High-end innovative tools and resources"
    },
    {
      img: "../images/wellteam/team_1.png",
      title: "Well-researched team"
    },
    {
      img: "../images/Eproactive/EAP_1.png",
      title: "Empathic and Proactive"
    }
  ]
}


const CTA = {
  img: "",
  title: "",
  desc1: "The secret to unveiling the future of the company is digital transformation. We have a robust culture of creativity, innovation and discovery at Technovature. We are continually seeking new approaches and abilities and embracing cutting-edge technology that might unlock the possibilities of a digital world.",
  desc2: "Let Technovature Technologize and Transform your enterprise.",
  btn: "Connect with us today! "
}
