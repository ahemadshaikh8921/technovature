import Header3 from "../../layout/header-3";
// import Image from "next/image";
import Seo from "../../element/seo";
import Slider3 from "../../element/slider-3";
import ContactForm from "../../element/contact-form";
import AboutUs3 from "../../element/aboutUs-3";
// import GetInTouch3 from "../element/get-in-touch";
import Team3 from "../../element/team-3";
import Blog3 from "../../element/blog-3";
import { useEffect, useState } from "react";
import { db, } from "../../firebase_config";
import Link from "next/link";
import { LazyLoadImage } from "react-lazy-load-image-component";
import FAQ from "../../element/faq";
import Features from "../../element/features";
import FeaturesComponents from "../../element/featuresComponents";
import Footer3 from "../../layout/footer-3";
import Connect_with_us from "../../element/connect_with_us";

function BlockChain() {
    const [blogData, setBlogData] = useState([]);
    useEffect(() => {
        getData();
    }, []); // blank to run only on first launch

    function getData() {
        db.collection("blog").onSnapshot(function (querySnapshot) {
            setBlogData(
                querySnapshot.docs.map((doc) => ({
                    title: doc.data().title,
                    author: doc.data().author,
                    content: doc.data().content,
                    featured_img: doc.data().featured_img,
                    metadesc: doc.data().metadesc,
                    slug: doc.data().slug,
                    keywords: doc.data().keywords,
                    date: doc.data().created_at,
                }))
            );
        });
    }

    return (
        <>
            <Seo title={SeoData.title} desc={SeoData.desc} canonical={SeoData.canonical} />
            {/* navbar header */}
            <Header3 />

            <div className="page-content bg-white" id="top">
                {/* Hero Section contains data & form */}
                <Slider3 {...BannerData} contact={<ContactForm />} form />

                {/* Automation */}
                {/* <AboutUs3  /> */}

                <Features title='Features' >

                    {
                        _Features.map((feature, id) => {
                            return (
                                <div key={id} className="col-lg-6 mt-2">
                                    <FeaturesComponents  {...feature} />
                                </div>
                            )
                        })
                    }

                </Features>

                {/* Making Considerate Choices */}
                <AboutUs3 bggray="bg-gray" {...ProcessOFWorking} style={{ flexDirection: "row-reverse", alignItems: "center" }} />

                {/* Featured Clients */}
                {/* <AboutUs3  {...AboutUsData} title="Featured Clients" featureClient /> */}

                {/* How it Works */}
                <AboutUs3   {...WhyChooseTechnovature} />

                {/* Meet Our Team */}
                <Team3 TeamData={TeamData} title />

                <Connect_with_us {...CTA} />


                {/* CTA -- Get in Touch Form Components */}
                {/* <GetInTouch3 test /> */}

                {/* FAQ */}
                <FAQ defaultAccordion={defaultAccordion} />

                {/* Featured Blog Posts */}
                <Blog3  >
                    {blogData.slice(0, 4).map((x, i) => {
                        return (
                            <div key={x.title + i} className="col-xl-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
                                <div className="dlab-blog blog-half m-b30">
                                    <div className="dlab-media">
                                        <Link href={`/blog/${x.slug}`}><a ><LazyLoadImage src={x.featured_img} alt="" /></a></Link>
                                    </div>
                                    <div className="dlab-info">
                                        <h3 style={{ fontSize: "1.5rem" }} className="dlab-title">
                                            <Link href={`/blog/${x.slug}`}><a>{x.title}</a></Link>
                                        </h3>
                                        <p className="m-b0 blog-desc-style"> <Link href={`/blog/${x.slug}`}><a style={{ textDecoration: "none !important", color: "#505489" }}>{x.metadesc}</a></Link></p>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </Blog3>
            </div>
            <Footer3 />
        </>
    )
}


export default BlockChain;




// Artificial Intelligence Page
const SeoData = {
    title: "Cloud Computing | Technovature Software Solutions Pvt. Ltd.",
    desc: "High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation",
    canonical: "https://www.technovature.com/services/cloud"
}
const TeamData = [
    { name: "Kalyani Jallapuram", img: "../images/team/small/kalyani.jpeg", position: "Director, Operations", dealy: "0.1s" },
    { name: "Srikanth Jallapuram", img: "../images/team/small/shrikanth.jpeg", position: "Founder Director & CEO", delay: "0.2s" },
]
const BannerData = {
    subtitle: "We provide the most creative cloud software solutions that are scalable and enable organizations to reap the benefits.",
    title: "Embrace the power of autonomous operations with Cloud Computing",
    desc: "You normally only pay for the cloud services you use, which lowers your operating costs, allows you to control your network more effectively, and allows you to scale as your company's needs change.",
    desc1: null,
    desc2: null,
    link: "/#contact-now"
}

const _Features = [
    {
        title: "Omnipresence",
        desc1: "A cloud service sells services to anybody with an internet connection. A private cloud is a private network or data center that hosts services for a small group of people with limited access and permissions. The goal of cloud computing, whether private or public, is to provide simple, scalable access to computer resources and information technology services.",
    },
    {
        title: "Unparalleled Speed",
        desc1: "Because most cloud computing services are self-service and on-demand, even huge amounts of computing resources may be deployed in minutes, usually with only a few mouse clicks, providing organizations with a lot of flexibility and capacity planning.",
    },
    {
        title: "High Efficiency",
        desc1: "The largest cloud computing services are delivered through a global network of secure data centers continually upgraded to the latest era of fast and efficient computer gear. This has various advantages over a single company data center, including lower network latency for apps and larger economies of scale.",
    },
]

const ProcessOFWorking = {
    img: "../newImg/about/blockchain-1.png",
    title: "Process of working ",
    // desc1:"Unfurling businesses’ digital aptitudes.",
    // desc2: <>We are “Digital Transforming Facilitators.” Bridging the gap of the digital needs of a business and accurate, custom, high-quality solutions, we offer our clients an array of tech-based services. At Technovature, we believe in scalability and sustainability, and hence our cutting-edge tailored solutions ensure grooming business “Future Ready.”</>,
    // desc2: "Is your organization geared up for digital transformation? Technovature empowers your business with cutting-edge technologies customized to accomplish your business goal.",
    // counterData: [
    //   { delay: "0.2s", title: "Years of experience",sign:"+", count: "5" },
    //   { delay: "0.4s", title: <>Projects<br/> handled</>,sign:"+", count: "100" },
    //   { delay: "0.6s", title: "Satisfied customers",sign:"+", count: "98" },
    // ],
    listData: [
        { content: "Our team will read and understand your requirement" },
        { content: "We will analyze and project a plan for you" },
        { content: "Displaying it in front of you for an approval " },
        { content: "Execution and development will start" },
    ]
}

const WhyChooseTechnovature = {
    img: "../newImg/about/blockchain-2.png",
    title: "Why choose Technovature?",
    desc1: "At Technovature, we think that a business should not only survive but also prosper in the impending Golden Digital Future period. To reach that purpose, we cooperate and work with the Business in unprecedented ways.",
    desc2: 'Cloud computing, in its most basic form, is the transmission of computer services such as servers, memory, databases, networking, software, analytics, and intelligence via the Internet ("the cloud") in order to deliver faster innovation, more flexible capabilities, and economies of scale.',
}

const CTA = {
    img: "",
    title: "",
    desc1: "The secret to unveiling the future of the company is digital transformation. We have a robust culture of creativity, innovation and discovery at Technovature. We are continually seeking new approaches and abilities and embracing cutting-edge technology that might unlock the possibilities of a digital world.",
    desc2: "Let Technovature Technologize and Transform your enterprise.",
    btn: "Connect with us today!"
}

const defaultAccordion = [
    {
        title: "What is Cloud Computing ?",
        text:
            "Cloud computing is the on-demand delivery of compute resources, applications and storage over the Internet. Computing and storage capacity can be provided to users on an as-needed basis, allowing businesses to only pay for the resources they use. Cloud computing also allows users to access applications and data from any device with an Internet connection, making it a perfect fit for today’s mobile workforce.",
        text1: "Cloud computing services can be delivered through a variety of models, including public, private and hybrid clouds. In a public cloud, resources are shared",    
        bg: "primary",
    },
    {
        title: "Benefits of Cloud Computing",
        text:
            "Cloud computing is a model for enabling convenient, on-demand network access to a shared pool of configurable computing resources (e.g., networks, servers, storage, applications, and services). Cloud computing and storage solutions provide users and organizations with a variety of benefits, such as on-demand self-service, rapid elasticity, and broad network access.",
        text1: 'On-demand self-service', 
        text2: 'Cloud computing allows users to access resources, such as applications and storage, on an as-needed basis. This eliminates the need to contact a system administrator or wait for an IT technician to provision resources. Users can simply log in to the cloud and start using the applications and storage they need.',
        text3: 'Rapid elasticity',
        text4: 'Cloud computing enables organizations to quickly and easily scale their computing resources up or down in response to changes in demand. For example, a company can increase the number of servers it uses during the holiday season to handle increased traffic. When demand decreases, the company',   

        bg: "info",
    },
    {
        title: "Types of Clouds",
        text:
            "There are three types of clouds: public, private, and hybrid.",
        text1: 'Public clouds are those that are offered by third-party providers and are accessible to anyone who has an internet connection. These clouds are usually less expensive than private clouds, but they are also less secure because they are open to anyone. ',
        text2: "Private clouds are those that are offered by a company's IT department and are accessible only to employees of that company. Private clouds are more expensive than public clouds, but they are also more secure because they are accessible only to authorized users.",  
        text3: 'Hybrid clouds are those that are a combination of public and private clouds. They are more expensive than public clouds, but they are also more secure because they are accessible only to authorized users.',  

        bg: "success",

    },
    {
        title: "Cloud Computing Services",
        text:
            "Cloud Computing Services are a model for enabling convenient, on-demand network access to a shared pool of configurable computing resources (e.g., networks, servers, storage, applications, and services). Cloud Computing Services allow organizations to consume compute resources as a utility – like electricity – rather than having to plan and procure these resources themselves. This on-demand access to resources can help organizations improve their business agility, efficiency, and competitiveness.",
        text1: 'Cloud Computing Services are delivered to consumers via the Internet, often through web-based interfaces. Cloud Computing Services can be used to host and manage business applications, store and share data, or provide other services that can be accessed by authorized users from anywhere at any time.',
        text2: "The three most common Cloud Computing Services models are Infrastructure as a Service (IaaS), Platform as a Service (PaaS), and Software as a Service (SaaS).",    

        bg: "success",
    },
    {
        title: "Security and Privacy in Cloud Computing",
        text:
            "Cloud computing is a model for enabling ubiquitous, convenient, on-demand network access to a shared pool of configurable computing resources (e.g., networks, servers, storage, applications, and services). Cloud computing and the cloud model are gaining popularity because they offer an attractive alternative to traditional client-server computing. In a cloud environment, users can access applications and services from any device with an Internet connection, making it possible to work from anywhere.",
        text1: 'Cloud computing also offers organizations the potential to reduce IT costs by consolidating resources and by making better use of underutilized infrastructure. Cloud services can be provisioned and terminated on demand, which gives organizations the flexibility to scale their services up or down as needed.',
        text2: "Although the cloud model offers many benefits, it also raises security and privacy concerns. In a cloud environment, users’ data is potentially accessible from any device with an Internet connection, raising the risk that confidential information could be compromised. Additionally, because cloud services",    

        bg: "success",
    },
    {
        title: "Issues and Challenges in Cloud Computing",
        text:
            "There are numerous benefits to using cloud computing, from cost savings to increased efficiency and agility. However, cloud computing also presents a number of challenges for businesses. Security, data governance, and compatibility are just a few of the issues that need to be considered when moving to the cloud.",
        text1: 'Security is a top concern for businesses when moving to the cloud. In order to ensure data security, businesses need to ensure that their cloud provider has implemented proper security measures, such as firewalls, intrusion detection systems, and data encryption. Businesses should also create a security plan that includes employee training, password policies, and data backup and recovery plans.',
        text2: 'Data governance is another important issue to consider when moving to the cloud. In order to ensure that data is properly managed and protected, businesses need to create a data governance plan that includes data classification schemes, retention policies, and procedures for accessing and sharing data.',    

        bg: "success",
    },
    {
        title: "Cloud Computing Applications",
        text:
            "Cloud computing is the latest buzzword in the technology industry. It is a way of delivering computing services over the internet. This allows businesses to reduce their IT costs by accessing these services on demand.",

        text1: 'There are a number of cloud computing applications that are available. These include:',

        text2: '-Software as a Service (SaaS)',
        text3: "-Platform as a Service (PaaS)",
        text4: "-Infrastructure as a Service (IaaS)",
        
        text5: "Software as a Service (SaaS) is a model where software is delivered over the internet. This allows businesses to access applications without having to install them on their own computers.",

        text6: "Platform as a Service (PaaS) is a model where businesses can rent a computing platform from a provider. This allows businesses to develop and deploy their own applications without having to worry about the underlying infrastructure.",

        text7: "Infrastructure as a Service (IaaS) is a model where businesses can rent computing resources from a provider.",

        bg: "success",
    },
    {
        title: "Cloud Computing Case Studies",
        text:
            "Cloud computing can be used to power a wide variety of applications, including enterprise applications, consumer applications, and mobile applications. In the enterprise, cloud computing can be used to reduce the cost and complexity of IT, to improve agility, and to increase innovation. In the consumer space, cloud computing can be used to improve the quality of life by providing on-demand access to information and entertainment. And in the mobile space, cloud computing can be used to provide anytime, anywhere access to applications and data.",

        bg: "success",
    },
    {
        title: "What to consider before migrating to Cloud Computing?",
        text:
            "Cloud Computing is a model for enabling ubiquitous, convenient, on-demand network access to a shared pool of configurable computing resources (e.g., networks, servers, storage, applications, and services).",
        text1: "Cloud Computing offers many potential benefits for businesses of all sizes, including:", 
        text2: "- Reduced Costs: By consolidating servers and storage, businesses can save money on capital and operating expenses.",
        text3: "- Increased Efficiency and Agility: Businesses can respond quickly to changing market conditions and customer demands by deploying applications and resources in the cloud.",
        text4: "- Enhanced Security: Cloud providers have significant experience and expertise in securing their infrastructure and protecting customer data.",
        text5: "Before migrating to Cloud Computing, businesses should carefully consider the following factors:",
        text6: "- Cloud Models: There are three primary models for delivering cloud services: Infrastructure as a Service (IaaS), Platform as a Service (PaaS), and Software as a Service (SaaS). businesses should choose the model that best fits their business needs.",
        text7: "Also, you need to make sure that your business actually needs the Cloud. Not every business needs to migrate to the Cloud, and in some cases, it may even be more expensive and less efficient to do so. If you don't have a lot of data or if you don't need to access your data from multiple locations, the Cloud may not be the best option for you.",
        text8: "Finally, you need to make sure that your infrastructure is ready for the Cloud. Many businesses are not ready for the Cloud, either because they lack the necessary infrastructure or because they don't have the right people in place to manage it. Before migrating to the Cloud, you need to make sure that you have the bandwidth, storage, and security in place to support it.",   

        bg: "success",
    },
    {
        title: "How to migrate to Cloud Computing?",
        text:
            "The cloud has been a buzzword for a few years now, but what does it mean for your business? And more importantly, how do you migrate to the cloud?",
        text1: "The cloud is essentially a way to outsource your IT needs. Rather than maintain your own servers and software, you can use the cloud to access what you need on an as-needed basis. This can be a great way to save on costs, as you only pay for the services you use.", 
        text2: "When it comes to migrating to the cloud, there are a few things to consider. First, you need to make sure that the cloud is a good fit for your business. Not every business is a good candidate for the cloud. You need to have a reliable internet connection, and your business should be able to function with minimal or no on-site IT staff",

        text3: "Migration to cloud computing is not as difficult as it seems. In fact, it can be a very smooth process with the right guidance. Here are the basic steps you need to take to migrate to the cloud:",

        text3UL: [
            "Assess your needs - The first step is to assess your needs. What are you looking to get out of the cloud? What are your business goals? What are your current IT needs?",
            "Choose the right cloud provider - Not all cloud providers are created equal. You need to choose the right provider that meets your needs. Do your research and ask around for recommendations.",
            "Plan your migration - The next step is to plan your migration. This includes figuring out your budget, timeline and resources needed.",
            "Migrate your data - The next step is to migrate your data. This can be a daunting task, but the right cloud provider can help make it a smooth process."
        ],
           
        bg: "success",
    },
    {
        title: "Cloud Computing Resources",
        text:
            "Cloud computing resources are those that allow users to access and use information and applications that are stored remotely. This allows for a more efficient and streamlined way of working, as users no longer have to store and maintain their own information. Instead, they can simply access it through the cloud.",

        text1: "There are a number of different cloud computing resources available, each with its own advantages and disadvantages. Some of the most popular include:", 

        text1UL: [
            "Google Drive: This is a cloud-based storage system that allows users to store and share files online. It is free to use for up to 5GB of storage, and includes a number of features such as the ability to collaborate on files with others, and to access files from any device.",
            "Dropbox: This is another cloud-based storage system that allows users to store and share files online. It offers 2GB of free storage, and has a number of features such as the ability to share files with others, and to access files",
            "Amazon S3 Storage: It is another enterprise grade data storage and real-time storage access system that provides developer API to build applications around it.",
        ],
           
        bg: "success",
    },
    {
        title: "Glossary of Cloud Computing Terms",
        text:
            "A",
        A: [
            "API (Application Programming Interface)",
            "- A set of rules and specifications that allow software to interact with other software. APIs act as a bridge between two applications, passing data back and forth and executing commands."
        ],
        
        text1: "B",
        B: [
            "Bandwidth- The total amount of data that can be transmitted over a network in a given period of time."
        ],

        text2: "C",
        C: [
            "Cloud Computing- A type of computing where data and applications are hosted in remote servers accessed via the Internet.",
            "Cloud Service Provider (CSP)- A company that provides cloud-based services, such as storage, computing power, or software.",
            "Cloud Storage- The ability to store data remotely in the cloud. This can include everything from individual files to entire applications."
        ],

        text3: "D",
        D: [
            "Data Center- A facility used to house computer systems and associated components, such as telecommunications and storage systems."
        ],

        text4: "E",
        E: [
            "Elasticity - The ability for a system to scale up or down"
        ],
           
        bg: "success",
    },
];