import Header3 from "../../layout/header-3";
// import Image from "next/image";
import Seo from "../../element/seo";
import Slider3 from "../../element/slider-3";
import ContactForm from "../../element/contact-form";
import AboutUs3 from "../../element/aboutUs-3";
// import GetInTouch3 from "../element/get-in-touch";
import Team3 from "../../element/team-3";
import Blog3 from "../../element/blog-3";
import { useEffect, useState } from "react";
import { db, } from "../../firebase_config";
import Link from "next/link";
import { LazyLoadImage } from "react-lazy-load-image-component";
import FAQ from "../../element/faq";
import Features from "../../element/features";
import FeaturesComponents from "../../element/featuresComponents";
import Footer3 from "../../layout/footer-3";
import Connect_with_us from "../../element/connect_with_us";

function Fintech() {
    const [blogData, setBlogData] = useState([]);
    useEffect(() => {
        getData();
    }, []); // blank to run only on first launch

    function getData() {
        db.collection("blog").onSnapshot(function (querySnapshot) {
            setBlogData(
                querySnapshot.docs.map((doc) => ({
                    title: doc.data().title,
                    author: doc.data().author,
                    content: doc.data().content,
                    featured_img: doc.data().featured_img,
                    metadesc: doc.data().metadesc,
                    slug: doc.data().slug,
                    keywords: doc.data().keywords,
                    date: doc.data().created_at,
                }))
            );
        });
    }

    return (
        <>
            <Seo title={SeoData.title} desc={SeoData.desc} canonical={SeoData.canonical} />
            {/* navbar header */}
            <Header3 />

            <div className="page-content bg-white" id="top">
                {/* Hero Section contains data & form */}
                <Slider3 {...BannerData} contact={<ContactForm />} form />

                {/* Automation */}
                {/* <AboutUs3  /> */}

                <Features title='Features' >

                    {
                        _Features.map((feature, id) => {
                            return (
                                <div key={id} className="col-lg-6 mt-2">
                                    <FeaturesComponents  {...feature} />
                                </div>
                            )
                        })
                    }

                </Features>

                {/* Making Considerate Choices */}
                <AboutUs3 bggray="bg-gray" {...ProcessOFWorking} style={{ flexDirection: "row-reverse", alignItems: "center" }} />

                {/* Featured Clients */}
                {/* <AboutUs3  {...AboutUsData} title="Featured Clients" featureClient /> */}

                {/* How it Works */}
                <AboutUs3   {...WhyChooseTechnovature} />

                {/* Meet Our Team */}
                <Team3 TeamData={TeamData} title />

                <Connect_with_us {...CTA} />


                {/* CTA -- Get in Touch Form Components */}
                {/* <GetInTouch3 test /> */}

                {/* FAQ */}
                <FAQ defaultAccordion={defaultAccordion} />

                {/* Featured Blog Posts */}
                <Blog3  >
                    {blogData.slice(0, 4).map((x, i) => {
                        return (
                            <div key={x.title + i} className="col-xl-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
                                <div className="dlab-blog blog-half m-b30">
                                    <div className="dlab-media">
                                        <Link href={`/blog/${x.slug}`}><a ><LazyLoadImage src={x.featured_img} alt="" /></a></Link>
                                    </div>
                                    <div className="dlab-info">
                                        <h3 style={{ fontSize: "1.5rem" }} className="dlab-title">
                                            <Link href={`/blog/${x.slug}`}><a>{x.title}</a></Link>
                                        </h3>
                                        <p className="m-b0 blog-desc-style"> <Link href={`/blog/${x.slug}`}><a style={{ textDecoration: "none !important", color: "#505489" }}>{x.metadesc}</a></Link></p>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </Blog3>
            </div>
            <Footer3 />
        </>
    )
}


export default Fintech;




// Artificial Intelligence Page
const SeoData = {
    title: "Web Development | Technovature Software Solutions Pvt. Ltd.",
    desc: "High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation",
    canonical: "https://www.technovature.com/services/fintech"
}
const TeamData = [
    { name: "Kalyani Jallapuram", img: "../images/team/small/kalyani.jpeg", position: "Director, Operations", dealy: "0.1s" },
    { name: "Srikanth Jallapuram", img: "../images/team/small/shrikanth.jpeg", position: "Founder Director & CEO", delay: "0.2s" },
]
const BannerData = {
    subtitle: null,
    title: "Experience the best for your Business with unparalleled web development services",
    desc: "The creation of a website is a means to inform visitors about the items and services you are providing and let them realize why they should buy or use your products and what makes your Business unique from rivals.",
    desc1: " It is crucial to make your product as relevant and alluring as possible by presenting this information with high-quality photos and a well-thought-out presentation.",
    desc2: "The work involved in creating websites for hosting commercial or personal web pages is a popular definition of web development. Design, layout, content, network security setup, and client-side/server-side scripting are the main components of web development. Finding the finest web development business to create and set up your website is crucial if you want a well-structured, optimized website.",
    link: "/#contact-now"
}

const _Features = [
    {
        title: "Web development permits online marketing ",
        desc1: "Newspapers and other conventional forms of advertising, such as radio and television, are costly and declining in this digital age.",
    },
    {
        title: "Web development generates a profit",
        desc1: "A profitable website could bring in more money than it spends. Remember that your website is available 24/7, allowing you the flexibility to conduct Business whenever you choose.",
    },
    {
        title: "Obtaining Beneficial Search Engine Traffic",
        desc1: "A website must be properly developed and optimized to attract high-quality search engine visitors.",
    }
]

const ProcessOFWorking = {
    img: "../newImg/about/About_us.svg",
    title: "Process of working ",
    // desc1:"Unfurling businesses’ digital aptitudes.",
    // desc2: <>We are “Digital Transforming Facilitators.” Bridging the gap of the digital needs of a business and accurate, custom, high-quality solutions, we offer our clients an array of tech-based services. At Technovature, we believe in scalability and sustainability, and hence our cutting-edge tailored solutions ensure grooming business “Future Ready.”</>,
    // desc2: "Is your organization geared up for digital transformation? Technovature empowers your business with cutting-edge technologies customized to accomplish your business goal.",
    // counterData: [
    //   { delay: "0.2s", title: "Years of experience",sign:"+", count: "5" },
    //   { delay: "0.4s", title: <>Projects<br/> handled</>,sign:"+", count: "100" },
    //   { delay: "0.6s", title: "Satisfied customers",sign:"+", count: "98" },
    // ],
    listData: [
        { content: "Our team will read and understand your requirement" },
        { content: "We will analyze and project a plan for you" },
        { content: "Displaying it in front of you for an approval " },
        { content: "Execution and development will start" },
    ]
}

const WhyChooseTechnovature = {
    img: "../newImg/about/about.svg",
    title: "Why choose Technovature?",
    desc1: "At Technovature, we think a business should survive and prosper in the impending Golden Digital Future period. To reach that purpose, we cooperate and work with the Business in unprecedented ways.",
    desc2: 'Cloud computing, in its most basic form, is the transmission of computer services such as servers, memory, databases, networking, software, analytics, and intelligence via the Internet ("the cloud") in order to deliver faster innovation, more flexible capabilities, and economies of scale.',
}

const CTA = {
    img: "",
    title: "",
    desc1: "The secret to unveiling the future of the company is digital transformation. We have a robust culture of creativity, innovation and discovery at Technovature. We are continually seeking new approaches and abilities and embracing cutting-edge technology that might unlock the possibilities of a digital world.",
    desc2: "Let Technovature Technologize and Transform your enterprise. ",
    btn: "Connect with us today! "
}

const defaultAccordion = [
    {
        title: "Latest Trends in Web Development",
        text:
            "The internet is constantly evolving and with it, so is web development. Here are some of the latest trends in web development that you need to know about.",

        text1UL: [
            "Progressive web apps - Progressive web apps are one of the hottest trends in web development right now. They are web apps that are designed to work like regular apps, but can also be used in a web browser. They are becoming more and more popular because they are faster and more reliable than regular web apps.",
            "Microservices - Microservices are another hot trend in web development. They are a type of architecture that breaks an app up into small, independent services. This makes them easier to manage and more scalable.",
            "Artificial intelligence -Artificial intelligence is another trend that is growing in popularity. It is being used more and more in web development to create more user-friendly and responsive apps.",
            "Chatbots - Chatbots are basically computer programs that can simulate a conversation with a human. They are commonly used to provide customer support or to answer questions on a website. They can be used to provide information about a product or to help users complete a task."
        ],
        bg: "success",
    },
    {
        title: "Development Tools:",
        text:
            "In the constantly changing and advancing world of web development, it's important to stay up-to-date on the latest tools and technologies. Here are some of the most recent tools and technologies you should be aware of:",

        text1: "Webpack: A module bundler for web applications, Webpack is a powerful tool that helps you build your project's dependencies and modules into one large file. This can be helpful when working with large projects, as it helps to keep everything organized and streamlined.",

        text2: "React: React is a popular JavaScript library for building user interfaces. It allows you to create reusable components so that your code is more organized and manageable. React is also known for its speed and efficiency, making it a popular choice for web development.",

        text3: "Vue.js: Vue.js is another popular JavaScript library, and is often compared to React. Vue.js is known for its simplicity and ease of use, making it a great choice for those new to web.",

        bg: "success",
    },
    {
        title: "Languages and Frameworks:",
        simple: [
            "There are many different web development languages and frameworks available today. In order to choose the right one for your project, you need to understand the difference between them.",
            "Languages",
            "Web development languages are used to create the code that makes up a website. There are many different languages to choose from, but some of the most popular ones include:",
            "-HTML: HTML is the most basic language of web development. It is used to create the structure of a website, and does not include any styling or functionality.",
            "-CSS: CSS is used to style HTML, adding color, fonts, and other features.",
            "-JavaScript: JavaScript is used to add functionality to websites, such as animations and interactivity.",
            "Frameworks",
            "Frameworks are sets of libraries and tools that make it easier to develop websites using specific languages. They provide templates, code snippets, and other tools that make it easier to create websites without having to write all the code from scratch",
            "There are many different frameworks to choose from, and it can be difficult to decide which one to use. In this article, we will compare two popular frameworks: AngularJS and ReactJS.",
            "AngularJS",
            "AngularJS is a JavaScript framework developed by Google. It is a full-featured framework that includes everything you need to get started with web development. It includes a templating engine, a model-view-controller (MVC) framework, and a library for manipulating the DOM.",
            "AngularJS is a popular framework, and there is a large community of developers who are familiar with it. This makes it a good choice for projects that require a lot of custom development.",
            "ReactJS is a JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers and companies. ReactJS lets you create reusable components so that your code is easy to read and maintain. When a user interacts with a ReactJS component, ReactJS updates the component automatically. This makes your app more responsive and fast.",
            "Ruby on Rails is a popular web development framework that is based on the Ruby programming language. One of the main advantages of using Ruby on Rails is that it is relatively easy to learn. Additionally, Ruby on Rails is mature and well-supported, and it has a large community of developers. However, Ruby on Rails is not as fast as some of the other frameworks, and it can be more difficult to find developers who are familiar with it."

        ],

        bg: "success",
    },
    {
        title: "Case Studies:",
        text:
            "1. Development of a custom content management system for a major publishing company.",

        text1: "The publishing company needed a custom content management system to manage their large and complex website. Our team created a custom system using the latest web development technologies. The system was easy to use and allowed the publishing company to manage their website effectively.",

        text2: "2. Development of a custom e-commerce system for a major retailer.",

        text3: "The retailer needed a custom e-commerce system to manage their online sales. Our team created a custom system using the latest web development technologies. The system was easy to use and allowed the retailer to manage their online sales effectively.",

        text4: "3. Development of a custom social networking system.",

        text5: "The social networking company needed a custom social networking system to manage their website. Our team created a custom system using the latest web development technologies. The system was easy to use and allowed the social networking company to manage their website effectively.",

        bg: "success",
    },
    {
        title: "Modern Web Development Resources:",
        text:
            "As a web developer, you know that keeping up with the latest trends and technologies is essential to your success. But where should you start? And more importantly, where can you find reliable, up-to-date information?",
        text1: "Here are some of our favorite resources for modern web development:",    

        text1UL: [
            "The Mozilla Developer Network - The Mozilla Developer Network (MDN) is one of the most comprehensive sources of information on web development available. It includes detailed tutorials on a wide range of topics, as well as references to specs and standards.",
            "Stack Overflow - Stack Overflow is a Q&A forum for developers. It’s a great place to find solutions to specific problems, as well as to learn about new technologies and techniques.",
            "The Web Developer’s Handbook - The Web Developer’s Handbook is a comprehensive resource for all things web development. It covers everything from basic HTML and CSS to more advanced",
        ],
        bg: "success",
    },
];