import Header3 from "../../layout/header-3";
// import Image from "next/image";
import Seo from "../../element/seo";
import Slider3 from "../../element/slider-3";
import ContactForm from "../../element/contact-form";
import AboutUs3 from "../../element/aboutUs-3";
// import GetInTouch3 from "../element/get-in-touch";
import Team3 from "../../element/team-3";
import Blog3 from "../../element/blog-3";
import { useEffect, useState } from "react";
import { db, } from "../../firebase_config";
import Link from "next/link";
import { LazyLoadImage } from "react-lazy-load-image-component";
import FAQ from "../../element/faq";
import Features from "../../element/features";
import FeaturesComponents from "../../element/featuresComponents";
import Footer3 from "../../layout/footer-3";
import Connect_with_us from "../../element/connect_with_us";

function Cloud() {
    const [blogData, setBlogData] = useState([]);
    useEffect(() => {
        getData();
    }, []); // blank to run only on first launch

    function getData() {
        db.collection("blog").onSnapshot(function (querySnapshot) {
            setBlogData(
                querySnapshot.docs.map((doc) => ({
                    title: doc.data().title,
                    author: doc.data().author,
                    content: doc.data().content,
                    featured_img: doc.data().featured_img,
                    metadesc: doc.data().metadesc,
                    slug: doc.data().slug,
                    keywords: doc.data().keywords,
                    date: doc.data().created_at,
                }))
            );
        });
    }

    return (
        <>
            <Seo title={SeoData.title} desc={SeoData.desc} canonical={SeoData.canonical} />
            {/* navbar header */}
            <Header3 />

            <div className="page-content bg-white" id="top">
                {/* Hero Section contains data & form */}
                <Slider3 {...BannerData} contact={<ContactForm />} form />

                {/* Automation */}
                {/* <AboutUs3  /> */}

                <Features title='Features' >

                    {
                        _Features.map((feature, id) => {
                            return (
                                <div key={id} className="col-lg-6 mt-2">
                                    <FeaturesComponents  {...feature} />
                                </div>
                            )
                        })
                    }

                </Features>

                {/* Making Considerate Choices */}
                <AboutUs3 bggray="bg-gray" {...ProcessOFWorking} style={{ flexDirection: "row-reverse", alignItems: "center" }} />

                {/* Featured Clients */}
                {/* <AboutUs3  {...AboutUsData} title="Featured Clients" featureClient /> */}

                {/* How it Works */}
                <AboutUs3   {...WhyChooseTechnovature} />

                {/* Meet Our Team */}
                <Team3 TeamData={TeamData} title />

                <Connect_with_us {...CTA}  />


                {/* CTA -- Get in Touch Form Components */}
                {/* <GetInTouch3 test /> */}

                {/* FAQ */}
                <FAQ defaultAccordion={defaultAccordion} />

                {/* Featured Blog Posts */}
                <Blog3 >
                    {blogData.slice(0, 4).map((x, i) => {
                        return (
                            <div key={x.title + i} className="col-xl-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
                                <div className="dlab-blog blog-half m-b30">
                                    <div className="dlab-media">
                                        <Link href={`/blog/${x.slug}`}><a ><LazyLoadImage src={x.featured_img} alt="" /></a></Link>
                                    </div>
                                    <div className="dlab-info">
                                        <h3 style={{ fontSize: "1.5rem" }} className="dlab-title">
                                            <Link href={`/blog/${x.slug}`}><a>{x.title}</a></Link>
                                        </h3>
                                        <p className="m-b0 blog-desc-style"> <Link href={`/blog/${x.slug}`}><a style={{ textDecoration: "none !important", color: "#505489" }}>{x.metadesc}</a></Link></p>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </Blog3>
            </div>
            <Footer3 />
        </>
    )
}


export default Cloud;


const defaultAccordion = [
    {
        title: "What is Blockchain technology?",
        text:
            "The blockchain technology is a distributed database technology which has been gaining a lot of traction lately because of its potential to revolutionize various industries. The technology allows for a secure, transparent and tamper-proof way of storing data across a distributed network of computers. This makes it an ideal technology for applications such as recording transactions, tracking assets or managing digital",
        bg: "success",
    },
    {
        title: "How does Blockchain work?",
        text:
            'A blockchain is a digital ledger of all cryptocurrency transactions. It is constantly growing as "completed" blocks are added to it with a new set of recordings. Each block contains a cryptographic hash of the previous block, a timestamp, and transaction data. Bitcoin nodes use the block chain to differentiate legitimate Bitcoin transactions from attempts to re-spend coins that have already been spent elsewhere.',
        bg: "success",
    },
    {
        title: "What are the benefits of Blockchain technology? ",
        
        text: "The blockchain is a distributed database that allows for a secure, transparent and tamper-proof way of recording transactions. It has the potential to revolutionize the way the world does business.",

        text1: "Some of the benefits of blockchain technology include:",

        text1UL: [
            "Increased security and transparency - The blockchain is a secure way of recording transactions. It is tamper-proof and transparent, meaning that everyone involved in the transaction can see what is happening.",

            "Increased efficiency - The blockchain can speed up transactions, as there is no need for a third party to verify them.",

            "Reduced costs - The blockchain can reduce costs as there is no need for intermediaries.",

            "Increased efficiency - The blockchain can speed up transactions, as there is no need for a third party to verify them.",

            "Increased trust - The blockchain builds trust as it is a secure"
        ],

        text2: "Applications of Blockchain technology: ",

        text2UL: [
            "Bitcoin and Blockchain: ",

            "The blockchain technology is one of the most discussed technologies of the past years. The technology is often associated with the Bitcoin cryptocurrency, but the blockchain technology has many more applications. The technology allows for a secure and transparent way of recording transactions.",
            
        ],
        text2UL_UL: [
            "The blockchain technology can be used for many different applications. For example, the technology can be used to store medical data. The technology can also be used to store data about ownership of assets. The blockchain technology can also be used to store data about voting.",
            "The blockchain technology can be used to store data about ownership of assets. The technology can be used to store data about the provenance of assets. The blockchain technology can also be used to store data about the history of the asset. The blockchain technology can also be used to store data about the current state of the asset.",
            "The blockchain technology can be used to store data about the voting process. The technology can be used to store data about who has voted."

        ],

        bg: "success",
    },
    {
        title: "The future of Blockchain:",

        text0UL: [
            "When it comes to the future of blockchain technology, there is certainly a lot of speculation. Some experts believe that blockchain will revolutionize the world economy, while others are not so sure. Here is a look at some of the potential applications of blockchain technology and what the future may hold.",

            "One of the most exciting potential applications of blockchain technology is its ability to streamline the process of conducting transactions. Currently, when two parties wish to conduct a transaction, they need to go through a third party such as a bank. This can be slow and expensive. With blockchain technology, transactions can be conducted directly between the two parties involved, which can speed up the process and save money.",

            "Another potential application of blockchain technology is its use in the creation of digital currencies. Bitcoin is the most well-known example of a digital currency that is based on blockchain technology. Digital currencies are not tied to any particular country or central bank, and they can be used to purchase goods and services online",
        ],
        
        bg: "success",
    },
];


// Artificial Intelligence Page
const SeoData = {
    title: "Blockchain | Technovature Software Solutions Pvt. Ltd.",
    desc: "High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation",
    canonical: "https://www.technovature.com/services/blockchain"
}
const TeamData = [
    { name: "Kalyani Jallapuram", img: "../images/team/small/kalyani.jpeg", position: "Director, Operations", dealy: "0.1s" },
    { name: "Srikanth Jallapuram", img: "../images/team/small/shrikanth.jpeg", position: "Founder Director & CEO", delay: "0.2s" },
]
const BannerData = {
    subtitle: "Experience the undisrupted technology of Blockchain and unleash the best of your data for a skyrocketing business operation ",
    title: "Unchain the true potential of your Business",
    desc: "The most significant promise of blockchain technology is its power as a platform for developing new generations of transactional apps that will allow users to create trust and retain high security for their data and operations. Its use is so prevalent that there were over 28 million blockchain wallet users globally as of September 2018. Since its inception, about 41 million users have used Blockchain wallets, with over 200 billion USD in financial transactions recorded. In addition, with blockchain technology reaching over 140 countries, many countries are investigating the possibility of adopting cryptocurrency.",
    desc1: null,
    desc2: null,
    link: "/#contact-now"
}

const _Features = [
    {
        title: "Tried and tested by multiple industries ",
        desc1: "Blockchain technology has been used in various sectors and applications due to its ability to record transactional data. It is being exploited to its full potential to develop enterprises on a vast scale.",
    },
    {
        title: "Secure authentication",
        desc1: "Your data is private and vital, and Blockchain has the potential to alter how your essential information is seen substantially. Blockchain prevents fraud and unlawful behavior by producing a record that cannot be changed and is encrypted end-to-end.",
    },
    {
        title: "Increased speed and efficiency",
        desc1: "Traditional paper-intensive procedures are time-consuming, vulnerable to human error, and frequently need third-party intervention. Transactions may be conducted more quickly and effectively by optimizing these procedures with Blockchain.",
    },
]

const ProcessOFWorking = {
    img: "../newImg/about/cloud-1.svg",
    title: "Process of working ",
    // desc1:"Unfurling businesses’ digital aptitudes.",
    // desc2: <>We are “Digital Transforming Facilitators.” Bridging the gap of the digital needs of a business and accurate, custom, high-quality solutions, we offer our clients an array of tech-based services. At Technovature, we believe in scalability and sustainability, and hence our cutting-edge tailored solutions ensure grooming business “Future Ready.”</>,
    // desc2: "Is your organization geared up for digital transformation? Technovature empowers your business with cutting-edge technologies customized to accomplish your business goal.",
    // counterData: [
    //   { delay: "0.2s", title: "Years of experience",sign:"+", count: "5" },
    //   { delay: "0.4s", title: <>Projects<br/> handled</>,sign:"+", count: "100" },
    //   { delay: "0.6s", title: "Satisfied customers",sign:"+", count: "98" },
    // ],
    listData: [
        { content: "Our team will read and understand your requirement" },
        { content: "We will analyze and project a plan for you" },
        { content: "Displaying it in front of you for an approval " },
        { content: "Execution and development will start" },
    ]
}

const WhyChooseTechnovature = {
    img: "../newImg/about/cloud-2.svg",
    title: "Why choose Technovature?",
    desc1: "At Technovature, we think that a business should not only survive but also prosper in the impending Golden Digital Future period. To reach that purpose, we cooperate and work with the Business in unprecedented ways.",
    desc2: 'Blockchain technology provides a collaborative platform for exchanging reliable data. Technovature offers simple options to use blockchain technology, including a cloud storage service, the on edition, and a supply chain SaaS application.',
}

const CTA = {
    img: "",
    title: "",
    desc1: "The secret to unveiling the future of the company is digital transformation. We have a robust culture of creativity, innovation and discovery at Technovature. We are continually seeking new approaches and abilities and embracing cutting-edge technology that might unlock the possibilities of a digital world.",
    desc2: "Let Technovature Technologize and Transform your enterprise. ",
    btn: "Connect with us today! "
}
