
import Footer3 from "./../layout/footer-3";
import Header3 from "./../layout/header-3";
import Seo from "../element/seo";
import Link from 'next/link';
import { db, storage } from "../firebase_config"
import { useState, useEffect } from "react";
import Markdown from 'markdown-to-jsx';

function Index3({ }) {
    // console.log("postImg", postImg)
    // let imgData = [];
    const [blogData, setBlogData] = useState([]);
    // const [imageUrl, setImageUrl] = useState([]);
    useEffect(() => {
        getData();
    }, []); // blank to run only on first launch

    // const ImgStoreFunc = (data) => {
    //     imgData.push(data)
    //     setImageUrl(imgData)
    // }

    // function getImg(img) {
    //     storage.ref(`${img}`) //name in storage in firebase console
    //         .getDownloadURL()
    //         .then((url) => {
    //             imgData.push(url)
    //             setImageUrl(imgData)
    //             // ImgStoreFunc(url);
    //         })
    //         .catch((e) => console.log('Errors while downloading => ', e));
    // }
    function sortFunction(date1, date2) {
        let first = new Date(date1.date.seconds)
        console.log(date1.date.seconds, "date1.date.seconds")
        let second = new Date(date2.date.seconds)
        var dateA = new Date(first).getTime();
        var dateB = new Date(second).getTime();
        return dateA > dateB ? 1 : -1;
    };
    const newArr = blogData.sort(sortFunction);
    const newBlogArr = newArr.reverse();

    //    new Date(x.date.seconds * 1000).toDateString()

    function getData() {
        db.collection("blog").onSnapshot(function (querySnapshot) {
            setBlogData(
                querySnapshot.docs.map((doc) => ({
                    title: doc.data().title,
                    author: doc.data().author,
                    content: doc.data().content,
                    // featured_img: storage.ref(doc.data().featured_img) //name in storage in firebase console
                    //     .getDownloadURL()
                    //     .then((url) => { ImgStoreFunc(url); }).catch((e) => console.log('Errors while downloading => ', e)),
                    featured_img: doc.data().featured_img,
                    // featured_img: getImg(doc.data().featured_img),
                    metadesc: doc.data().metadesc,
                    slug: doc.data().slug,
                    keywords: doc.data().keywords,
                    date: doc.data().created_at,
                }))
            );
        });
    }

    // console.log(blogData[0].keywords, "checking");



    const SeoData = {
        title: 'Blog | Technovature Software Solutions Pvt. Ltd.',
        desc: 'High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation',
        canonical: 'https://www.technovature.com/blog/'
    }


    return (
        <>
            <Seo {...SeoData} />
            <Header3 />
            <div className="page-content bg-white" id="top">
                {/* <div className="dlab-bnr-inr style-1 bg-primary" style={{ backgroundImage: "url(images/banner/bnr2.png), var(--gradient-sec)", backgroundSize: "cover, 200%" }}>
                    <div className="container">
                        <div className="dlab-bnr-inr-entry">
                            <h1>Blog</h1>
                        </div>
                    </div>
                </div> */}
                <div className="section-head style-3 text-center mb-2 mt-4">
                    <h2 className="title">Blog</h2>
                    <div className="dlab-separator style-2 bg-primary"></div>
                    {/* <p style={{ marginBottom: "20px" }}>{desc}</p> */}
                </div>
                {/* Blog Section */}
                <section className="content-inner" style={{ "backgroundImage": "url(images/background/bg2.png)" }}>
                    <div className="container">
                        <div className="row">
                            {newBlogArr.map((x, i) => {
                                return (
                                    <div key={x.title} className="col-lg-4 col-md-6 wow fadeInUp" style={{padding:'20px'}} data-wow-duration="2s" data-wow-delay="0.2s">
                                        <div className="dlab-blog style-1 m-b50 bg-white">
                                            <div className="dlab-media dlab-img-effect zoom">
                                                <Link href={`/blog/${x.slug}`}><a ><img className="blog-img-style" src={x.featured_img} alt="" /></a></Link>
                                            </div>
                                            <div className="dlab-info">
                                                <div className="dlab-meta">
                                                    <ul>
                                                        {/* <li className="post-date"><i className="flaticon-clock m-r10"></i>{new Date(x.date.seconds * 1000).toDateString()}</li> */}
                                                        {/* <li className="post-author"><i className="flaticon-user m-r10"></i>By  {x.author}</li> */}
                                                        <li className="tag">{x.keywords}</li>

                                                        {/* <li className="post-comment"><a href="#"><i className="flaticon-speech-bubble"></i><span>15</span></a></li> */}
                                                        {/* <li className="post-share"><i className="flaticon-share"></i>
                                                            <ul>
                                                                <li><a className="fa fa-facebook" href="#"></a></li>
                                                                <li><a className="fa fa-twitter" href="#"></a></li>
                                                                <li><a className="fa fa-linkedin" href="#"></a></li>
                                                            </ul>
                                                        </li> */}
                                                    </ul>
                                                </div>
                                                <div className="" style={{height:'73px'}}>
                                                <h2 style={{ fontSize: "1.5rem" }} className="dlab-title">
                                                    <Link href={`/blog/${x.slug}`}><a >{x.title}</a></Link>
                                                </h2>
                                                </div>
                                                <p className="m-b0 blog-desc-style">{x.metadesc}</p>
                                                {/* <Markdown className="m-b0 blog-desc-style">{x.content}</Markdown> */}

                                                <div className="dlab-meta-data mt-2">
                                                    <ul>
                                                        <li className="post-date">{new Date(x.date.seconds * 1000).toDateString()}</li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </section>
            </div>
            <Footer3 />
        </>
    );
}

export default Index3;





