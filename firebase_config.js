import firebase from "firebase"
var firebaseConfig = {
    apiKey: "AIzaSyDbXeiwKd4IwNNQnoZTw1EiScAaW8NYay8",
    authDomain: "technovature-19a8a.firebaseapp.com",
    projectId: "technovature-19a8a",
    storageBucket: "technovature-19a8a.appspot.com",
    messagingSenderId: "779495117778",
    appId: "1:779495117778:web:cc38e22f769f6b8d55ee0b",
    measurementId: "G-2BP106BZZV"
};
if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

const db = firebase.firestore();
const storage = firebase.storage();
export { db ,storage};