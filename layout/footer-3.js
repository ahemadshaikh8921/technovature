import Link from 'next/link';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { MdFacebook } from "react-icons/md";
import { AiOutlineTwitter, AiOutlineInstagram } from "react-icons/ai";
import { FaLinkedin } from "react-icons/fa";
import { TfiEmail } from "react-icons/tfi";
import { IoCallOutline } from "react-icons/io5";
import { CiLocationOn } from "react-icons/ci";

function Footer3() {
	return (
		<>
			<footer className="site-footer style-3" id="footer" style={{ "backgroundSize": "cover, 200%",marginTop: '50px' }}>
				<div className="footer-top">
					<div className="container">
						<div className="row">
							<div className="col-xl-4 col-lg-12 col-md-4 col-sm-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
								<div className="widget widget_about">
									<div className="footer-logo"><Link href="/"><a><LazyLoadImage style={{ width: "150px" }} src="/newImg/logo2.png" alt="" /></a></Link></div>
									<p>Digital Transformation is the key to unlocking the future of business. At Technovature, we have established a strong culture of Innovation and Exploration. </p>
									<div className="social_links">
									<div className="facebook">
									<Link href="https://www.facebook.com/technovature"><a ><MdFacebook /></a></Link>
					                </div>
									<div className="twitter">
									<Link href="https://twitter.com/technovature?s=21&t=pZj4_i-CBOfMD0Z5zwnaGw"><a ><AiOutlineTwitter /></a></Link>
					                </div>
									<div className="instagram">
									<Link href=""><a ><AiOutlineInstagram /></a></Link>
					                </div>
									<div className="linkedin">
									<Link href="https://www.linkedin.com/company/technovature-software-solutions-pvt-ltd-/"><a ><FaLinkedin /></a></Link>
					                </div>
									</div>
								</div>
							</div>
							<div className="col-xl-2 col-lg-3 col-md-4  col-sm-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.4s">
								<div className="widget widget_services style-1">
									{/* <div className="footer-title">Our links</div> */}
									<ul>
										<li><Link href="/"><a>Home</a></Link></li>
										<li><Link href="/about"><a>About Us</a></Link></li>
										<li><Link href="/services"><a>Services</a></Link></li>
										<li><Link href="/blog"><a>Blog</a></Link></li>
										<li><Link href="/contact"><a>Contact Us</a></Link></li>
									</ul>
								</div>
							</div>
							<div className="col-xl-3 col-lg-3 col-md-4  col-sm-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.6s">
								<div className="widget widget_services style-1">
									{/* <div className="footer-title">Our Services</div> */}
									<ul>
										{/* <li><Link href="/services/5g"><a>5G Services</a></Link></li> */}
										{/* <li><Link href="/services/autonomous"><a>Autonomous Technology</a></Link></li> */}
										<li><Link href="/services/blockchain"><a>Blockchain</a></Link></li>
										<li><Link href="/services/cloud"><a>Cloud Computing</a></Link></li>
										<li><Link href="/services/bigdata"><a>Data Sciences</a></Link></li>
										<li><Link href="/services/iot"><a>Internet of Things</a></Link></li>
										<li><Link href="/services/ai"><a>Artificial Intelligence</a></Link></li>
										<li><Link href="/services/web_development"><a>Web Development</a></Link></li>
										{/* <li><Link href="/services/fintech"><a>Fintech Development</a></Link></li> */}
										{/* <li><Link href="/services/mobile"><a>Mobile Technologies</a></Link></li> */}
										{/* <li><Link href="/services/xr"><a>Extended Reality</a></Link></li> */}
									</ul>
								</div>
							</div>
							<div className="col-xl-3 col-lg-3 col-md-4  col-sm-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="1.0s">
								<div className="widget widget_getintuch">
									{/* <div className="footer-title">Contact Us</div> */}
									<ul>
										<li><i><IoCallOutline style={{color:'white'}} /></i><span>+91 7013175234</span></li>
										<li><i><TfiEmail style={{color:'white'}} /></i><span>info@technovature.com</span></li>
										<li style={{ alignItems: "flex-start" }}><i><CiLocationOn /></i><span>Plot No. 18, Level 2, iLabs Centre, Oval Building, Madhapur, Hitech City, Hyderabad 500081 Telangana, India</span></li>
									</ul>
								</div></div></div></div></div>
				{/* <!-- footer bottom part --> */}
				<div className="footer-bottom"><div className="container"><div className="row align-items-center"><div className="col-md-12 text-center"><span className="copyright-text">Copyright © 2022, Technovature Software Solutions Pvt. Ltd.</span></div></div></div></div>
			</footer>
		</>
	)
}

export default Footer3;