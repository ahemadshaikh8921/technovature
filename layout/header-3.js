
import Link from 'next/link';
import { useState } from 'react';
import { LazyLoadImage } from "react-lazy-load-image-component";
function Header3() {
    const [show, setShow] = useState(false);
    const [open, setOpen] = useState("home");
    return (
        <>
            {/* <!-- Header --> */}
            <header className="site-header mo-left">
                {/* header-transparent */}
                {/* <!-- Main Header --> */}
                <div className="sticky-header main-bar-wraper navbar-expand-lg">
                    <div className="main-bar clearfix ">
                        <div className="container clearfix">
                            {/* <!-- Website Logo --> */}
                            <div className="logo-header mostion logo-dark">
                                <Link href="/">
                                    <a>
                                        <LazyLoadImage style={{ height: "auto", objectFit: "cover", width: "75%" }} className="custom-logo-white" src="/images/logo-1.png" alt="" />
                                        <LazyLoadImage style={{ height: "80px", width: "auto" }} className="custom-logo" src="/images/logo-1.png" alt="" />
                                    </a>
                                </Link>

                            </div>
                            {/* <!-- Nav Toggle Button --> */}
                            <button className={`navbar-toggler collapsed navicon justify-content-end ${show ? "open" : ""}`} onClick={() => setShow(!show)}>
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>

                            {/* navbar links */}
                            <div style={{ paddingLeft: '20%' }} className={`header-nav navbar-collapse d-flex collapse justify-content-end  ${show ? "show" : ""}`} id="navbarNavDropdown">
                                <div style={{ width: '100%' }} id='anothernavbarNavDropdown' className='d-flex justify-content-between align-items-center'>
                                    <div className="">
                                        <div className="logo-header">
                                            <Link href="/"><a><LazyLoadImage src="/newImg/logo.png" alt="" /></a></Link>
                                        </div>
                                        <ul className="nav navbar-nav navbar">
                                            <li><Link href="/"><a>Home</a></Link></li>
                                            <li><Link href="/about"><a>About Us</a></Link></li>
                                            <li className={`${open === "services" ? "open" : ""}`}><Link href="/services/temp"><a onClick={() => setOpen("services")}>Services<i className="fa fa-chevron-down"></i></a></Link>
                                                <ul className="sub-menu" style={{ width: "250px" }}>
                                                    {/* <li><Link href="/services/5g/"><a>5G Services</a></Link></li> */}
                                                    <li><Link href="/services/ai/"><a>Artificial Intelligence</a></Link></li>
                                                    {/* <li><Link href="/services/autonomous/"><a>Autonomous Technology</a></Link></li> */}
                                                    <li><Link href="/services/iot/"><a>Internet Of Things</a></Link></li>
                                                    <li><Link href="/services/bigdata/"><a>Big Data</a></Link></li>
                                                    <li><Link href="/services/blockchain/"><a>Blockchain Development</a></Link></li>
                                                    <li><Link href="/services/cloud/"><a>Cloud Computing</a></Link></li>
                                                    <li><Link href="/services/web_development/"><a>Web Development</a></Link></li>
                                                    {/* <li><Link href="/services/web/"><a>Web Development</a></Link></li> */}
                                                    {/* <li><Link href="/services/xr/"><a>Extended Reality</a></Link></li> */}
                                                </ul>
                                            </li>
                                            <li><Link href="/blog"><a>Blog</a></Link></li>

                                        </ul>
                                    </div>
                                    <div className="newBtnContactNavbar">
                                        <Link href="/contact"><a id='navbarContactBTN' className='sameDesignBtn btn btn-corner gradient btn-primary'>Contact Us</a></Link>
                                    </div>
                                </div></div>
                        </div>
                    </div>
                </div>
                {/* <!-- Main Header End --> */}
            </header>
            {/* <!-- Header End --> */}

        </>
    )
}

export default Header3;

