import Image from "next/image";

export  const SeoData={
  title:"Home | Technovature Software Solutions Pvt. Ltd.",
  desc:"High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation",
  canonical:"https://www.technovature.com/"
}

export const BannerData = {
  subtitle: "Where Innovative CloudTechnology meets Business",
  title: "Harness AI and Cloud Native Services to take your Business to a New High",
  desc: "Every business needs adaptation to an effective core digital transformation strategy in place that sets itself apart from the rest. But does it deliver superior customer experiences and do you have the essential technology footprint that aligns with your business strategy? Get your custom-tailored business transformation delivered.",
  link: "/#contact-now"
}

export const TestBannerData = {
  subtitle: null,
  title: "Harness AI and Cloud Native Services to take your Business to a New High",
  desc: "Every business needs adaptation to an effective core digital transformation strategy in place that sets itself apart from the rest. But does it deliver superior customer experiences and do you have the essential technology footprint that aligns with your business strategy? Get your custom-tailored business transformation delivered.",
  link: "/#contact-now"
}

export const ServiceSection = {
  title: "Our Specialization",
  desc:"Your future is bright and boundless with an Innovation driven technology blue printand a partner that delivers it.",
  serviceData: [
    { singleLink: "/services/ai/", icon: "/newImg/serviceIcon/ai.png", title1: "Artificial", title2: "Intelligence", desc: "Unlock the growth potential of your business with AI", delay: "0.2s" },
    // { singleLink: "/services/5g/", icon: "/newImg/serviceIcon/5g_service.png", title1: "5G", title2: "Services", desc: "Unleash the ubiquitous and unlimited capacities of 5G services", delay: "0.4s" },
    // { singleLink: "/services/autonomous/", icon: "/newImg/serviceIcon/car.png", title1: "Autonomous", title2: "Technology", desc: "Shore Up your business growth with autonomous technology", delay: "0.6s" },
    { singleLink: "/services/bigdata/", icon: "/newImg/serviceIcon/dataScience.png", title1: "Big",  title2: "Data", desc: "Leverage data science to boost your business profitability", delay: "0.2s" },
    { singleLink: "/services/blockchain/", icon: "/newImg/serviceIcon/blockchain.png", title1: "Blockchain", title2: "Development", desc: "Incite transforming your business by channeling Blockchain development.", delay: "0.4s" },
    { singleLink: "/services/cloud/", icon: "/newImg/serviceIcon/cloud-data.png", title1: "Cloud", title2: "Computing", desc: "Delve into innovative cloud computing to flare up your business growth.", delay: "0.6s" },
    { singleLink: "/services/iot/", icon: "/newImg/serviceIcon/fintech.png", title1: "Internet Of",  title2: "Things", desc: "Cutting-edge technologies with the best user experience for seamless fintech business.", delay: "0.6s" },
    // { singleLink: "/services/mobile/", icon: "/newImg/serviceIcon/mobile-app.png", title1: "Mobile", title2: "Technology", desc: "Advanced to the futuristic mobile technology to stay ahead of your peers.", delay: "0.6s" },
    { singleLink: "/services/web/", icon: "/newImg/serviceIcon/web-development.png", title1: "Web", title2: "Development", desc: "Convergence of avant-garde technology and binge-worthy user experience.", delay: "0.6s" },
    // { singleLink: "/services/xr/", icon: "/newImg/serviceIcon/vr-glasses.png", title1: "Extended", title2: "Reality", desc: "Amplify your business agility with extended reality.", delay: "0.6s" },
  ],
  link: "/services"
}

export const AboutUsData = {
  img: "newImg/about/whyOurAgency.png",
  title: "Why Our Agency",
  desc1: "The foremost drivers of digital transformation are market competition and the role of AI to deliver to your customer expectations. Is your organization geared up for digital transformation? Technovature empowers your business with cutting-edge technologies customized to accomplish your business goal.",
  // desc2: "Is your organization geared up for digital transformation? Technovature empowers your business with cutting-edge technologies customized to accomplish your business goal.",
  counterData: [
    { delay: "0.2s", title: <>Years of Project Experience</>,sign:"+", count: "100" },
    { delay: "0.4s", title: <>Projects handled</>,sign:"+", count: "100" },
    { delay: "0.6s", title: <>Satisfied customers</>,sign:"%", count: "98" },
  ],
  p100: 'p100',
}

export const TestAboutUsData = {
  img: "newImg/about/about.svg",
  title: "Why Our Agency",
  desc1: "The foremost drivers of digital transformation are market competition and the role of AI to deliver to your customer expectations.",
  desc2: "Is your organization geared up for digital transformation? Technovature empowers your business with cutting-edge technologies customized to accomplish your business goal.",
}

export const AboutUsData2 = {
  img: "newImg/about/whoAreWe.png",
  title: "Who We Are",
  desc1:"Unfurling businesses’ digital aptitudes.",
  desc2: <>We are “Digital Transforming Facilitators.” Bridging the gap of the digital needs of a business and accurate, custom, high-quality solutions, we offer our clients an array of tech-based services. At Technovature, we believe in scalability and sustainability, and hence our cutting-edge tailored solutions ensure grooming business “Future Ready.”</>,
  // desc2: "Is your organization geared up for digital transformation? Technovature empowers your business with cutting-edge technologies customized to accomplish your business goal.",
  // counterData: [
  //   { delay: "0.2s", title: "Years of experience",sign:"+", count: "5" },
  //   { delay: "0.4s", title: <>Projects<br/> handled</>,sign:"+", count: "100" },
  //   { delay: "0.6s", title: "Satisfied customers",sign:"+", count: "98" },
  // ],
  listData:[
    {content:"100+ Years of Project Experience"},
    {content:"100+ Projects handled"},
    {content:"98% Satisfied customers"},
    {content:"Great team of highly experienced professionals"},
    {content:"Tailored solution by tech experts"},
  ]
}


export const TeamData = [
  { name: "Kalyani Jallapuram", img: "images/team/small/kalyani.jpeg", position: "Director, Operations", dealy: "0.1s" },
  { name: "Srikanth Jallapuram", img: "images/team/small/shrikanth.jpeg", position: "Founder Director & CEO", delay: "0.2s" },
]


export const portfolioData = [
  {
    title: "Software Landing Page Design",
    category: ["all", "cloud", "blockchain", "others"],
    img: (
      <Image
        src="/images/projects/project-1/pic1.jpg"
        layout="responsive"
        width={370}
        height={370}
      />
    ),
    desc: "With no technical staff in the business, this medical organization teamed up with DP Solutions to manage their technology in 2010. When the organization came on as a client, only a handful of employees had email, and those that did were using personal email accounts for the business.",
  },
  {
    title: "Software Landing Page Design",
    category: ["all", "ai", "blockchain"],
    img: (
      <Image
        src="/images/projects/project-1/pic2.jpg"
        layout="responsive"
        width={370}
        height={370}
      />
    ),
    desc: "With no technical staff in the business, this medical organization teamed up with DP Solutions to manage their technology in 2010. When the organization came on as a client, only a handful of employees had email, and those that did were using personal email accounts for the business.",
  },
  {
    title: "Software Landing Page Design",
    category: ["all", "cloud", "mobile-5g"],
    img: (
      <Image
        src="/images/projects/project-1/pic3.jpg"
        layout="responsive"
        width={370}
        height={370}
      />
    ),
    desc: "With no technical staff in the business, this medical organization teamed up with DP Solutions to manage their technology in 2010. When the organization came on as a client, only a handful of employees had email, and those that did were using personal email accounts for the business.",
  },
  {
    title: "Software Landing Page Design",
    category: ["all", "cloud", "blockchain"],
    img: (
      <Image
        src="/images/projects/project-1/pic4.jpg"
        layout="responsive"
        width={370}
        height={370}
      />
    ),
    desc: "With no technical staff in the business, this medical organization teamed up with DP Solutions to manage their technology in 2010. When the organization came on as a client, only a handful of employees had email, and those that did were using personal email accounts for the business. ",
  },
  {
    title: "Software Landing Page Design",
    category: ["all", "ai", "mobile-5g"],
    img: (
      <Image
        src="/images/projects/project-1/pic5.jpg"
        layout="responsive"
        width={370}
        height={370}
      />
    ),
    desc: "With no technical staff in the business, this medical organization teamed up with DP Solutions to manage their technology in 2010. When the organization came on as a client, only a handful of employees had email, and those that did were using personal email accounts for the business. ",
  },
  {
    title: "Software Landing Page Design",
    category: ["all", "banking-fintech", "blockchain", "others"],
    img: (
      <Image
        src="/images/projects/project-1/pic6.jpg"
        layout="responsive"
        width={370}
        height={370}
      />
    ),
    desc: "With no technical staff in the business, this medical organization teamed up with DP Solutions to manage their technology in 2010. When the organization came on as a client, only a handful of employees had email, and those that did were using personal email accounts for the business. ",
  },
];

