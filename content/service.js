export const ServiceSection = {
	title: "Our Speciallization",
	serviceData: [
		{ singleLink: "/services/ai/", icon: "/newImg/serviceIcon/ai.png", title: "Artificial Intelligence", desc: "Unlock the growth potential of your business with AI", delay: "0.2s" },
		{ singleLink: "/services/5g/", icon: "/newImg/serviceIcon/connectivity.png", title: "5G Services", desc: "Unleash the ubiquitous and unlimited capacities of 5G services", delay: "0.4s" },
		{ singleLink: "/services/autonomous/", icon: "/newImg/serviceIcon/car.png", title: "Autonomous Technology", desc: "Shore Up your business growth with autonomous technology", delay: "0.6s" },
		{ singleLink: "/services/bigdata/", icon: "/newImg/serviceIcon/dataScience.png", title: "Data Science", desc: "Leverage data science to boost your business profitability", delay: "0.2s" },
		{ singleLink: "/services/blockchain/", icon: "/newImg/serviceIcon/blockchain.png", title: "Blockchain Development", desc: "Incite transforming your business by channeling Blockchain development.", delay: "0.4s" },
		{ singleLink: "/services/cloud/", icon: "/newImg/serviceIcon/cloud-data.png", title: "Cloud Computing", desc: "Delve into innovative cloud computing to flare up your business growth.", delay: "0.6s" },
		{ singleLink: "/services/fintech/", icon: "/newImg/serviceIcon/fintech.png", title: "Fintech Development", desc: "Cutting-edge technologies with the best user experience for seamless fintech business.", delay: "0.6s" },
		{ singleLink: "/services/mobile/", icon: "/newImg/serviceIcon/mobile-app.png", title: "Mobile Technology", desc: "Advanced to the futuristic mobile technology to stay ahead of your peers.", delay: "0.6s" },
		{ singleLink: "/services/web/", icon: "/newImg/serviceIcon/web-development.png", title: "Web Development", desc: "Convergence of avant-garde technology and binge-worthy user experience.", delay: "0.6s" },
		{ singleLink: "/services/xr/", icon: "/newImg/serviceIcon/vr-glasses.png", title: "Extended Reality", desc: "Amplify your business agility with extended reality.", delay: "0.6s" },
	   ],
}