import { Fragment } from "react";
export const SeoData = {
    title: "About | Technovature Software Solutions Pvt. Ltd.",
    desc: "High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation",
    canonical: "https://www.technovature.com/about/"
}

export const AboutUsData = {
    img: "newImg/about/aboutus-about1.png",
    title: "About Us",
    desc1: "Digital Transformation is the key to unlocking the future of business. At Technovature, we have established a strong culture of Innovation and Exploration. We constantly strive to discover new methods and acquire new skills while adopting ground-breaking technology that can unlock the potential of a digital future.",
    desc2: " At Technovature, we believe that an Enterprise should not only survive but thrive in an upcoming era of Golden Digital Future. To achieve that goal we partner and collaborate with the Enterprise in ways not seen before.",

}

export const OurMissionData = {
    img: "newImg/about/OurMission2.png",
    title: "Our Mission",
    desc1: "With an Innovation and Solution centric mindset",
    desc2: (<Fragment>1. We enable and empower enterprises discover and uncover the potential of a solid digital future. <br /></Fragment>),
    desc3: (<Fragment>2. To forge a strong partnership for collaboration and cooperation with the Enterprise in achieving superior results.</Fragment>),

}

export const TeamData = [
    { name: "Kalyani Jallapuram", img: "images/team/small/kalyani.jpeg", position: "Director, Operations", dealy: "0.1s" },
    { name: "Srikanth Jallapuram", img: "images/team/small/shrikanth.jpeg", position: "Founder Director & CEO", delay: "0.2s" },
]