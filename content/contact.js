export const LocationData={
    img:"newImg/contact/loc.png",
    title:"Our Locations",
    desc1:"Plot No. 18, Level 2, iLabs Centre, Oval Building, Madhapur, Hitech City, Hyderabad 500081 Telangana, India",
    locationName:"Hyderabad",
    link:"/",
    email:"info@technovature.com",
    phone:"+91 7013175234",
  }
  
 export  const SeoData={
    title:"Contact | Technovature Software Solutions Pvt. Ltd.",
    desc:"High Technology Digital and Software Development company in Mobile, cloud, Big Data, Internet of Things, IoT, Data Sciences, Artificial Intelligence and Software Automation",
    canonical:"https://www.technovature.com/contact/"
  }