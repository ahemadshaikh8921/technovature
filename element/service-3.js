import { useState } from "react";
import Link from 'next/link';
import { LazyLoadImage } from "react-lazy-load-image-component";
import Slider from "react-slick";
import Aos from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";

function Service3({ title, serviceData, link, desc }) {
	console.log(serviceData,)
	const [open, setOpen] = useState("p2")
	let openData = ["p1", "p2", "p3"]
	useEffect(() => {
		Aos.init({ duration: 2000 });
	}, [])
	const settings = {
		dots: false,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		autoplay: false,
		arrows: true,
		speed: 5000,
		responsive: [
			{
				breakpoint: 1000,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					infinite: true,
				},
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
				},
			},
		],
	};

	const ParaStyle = {
		textOverflow: "ellipsis",
		display: " -webkit-box",
		whiteSpace: "initial",
		"-webkit-line-clamp": 3,
		"-webkit-box-orient": "vertical",
		overflow: "hidden",
		height: "65px",
		fontWeight: '400',
		textAlign: 'center',
		paddingTop: '10px'
	}
	return (
		<>
			{/* <!-- Service --> */}
			<section className="content-inner">
				<div data-aos="fade-up" className="container" style={{ width: '100%', overflow: 'hidden' }}>
					{title ? <div className="section-head style-3 text-center mb-4">
						<h2 className="title">{title}</h2>
						<div className="dlab-separator style-2 bg-primary"></div>
						<p style={{ marginBottom: "20px" }}>{desc}</p>
					</div> : null}
					{/* <div className="row"> */}
					{/* <div className="" style={{ width: '100%', overflow: 'hidden' }}> */}
					<Slider {...settings}>
						{serviceData.map((x, i) =>
								<Link href={x.singleLink}>
							<div className="sliderOutside" >

									{/* icon  */}
									<div className="icon-box">
										<LazyLoadImage src={x.icon} />
									</div>

									{/* details */}
									<div className={`mainSliderBox ${open === [openData[i]] ? "icon-bx-wraper dlab-blog style-1 style-5 box-hover text-center m-b30 active" : "icon-bx-wraper dlab-blog style-1 style-5 box-hover text-center m-b30"}`} onMouseOver={() => setOpen("p1")}>
										<h3 style={{ fontSize: "1.5rem", textAlign: 'center' }} id="addHeadingStyle" className=" dlab-title m-b15">{x.title1} <br /> {x.title2}</h3>
										<p className="m-b20" style={ParaStyle}>{x.desc}</p>
										{x.singleLink ? <Link href={x.singleLink}><a id="sliderBtn" className="btn btn-outline-primary"><i className="fa fa-angle-right"></i></a></Link> : null}
									</div>

									{/* <div id="singleSliderCom" key={x.title + i} data-wow-duration="2s" data-wow-delay={x.delay}>
							<Link href={x.singleLink}>
								<div className="">
									<a style={{ textDecoration: "none !important", color: "#505489" }}>
										<div style={{ width: '100%' }} className={`${open === [openData[i]] ? "icon-bx-wraper dlab-blog style-1 style-5 box-hover text-center m-b30 active" : "icon-bx-wraper dlab-blog style-1 style-5 box-hover text-center m-b30"}`} onMouseOver={() => setOpen("p1")}>
											<div className="icon-bx">
												<span className="icon-cell"><i><LazyLoadImage src={x.icon} /></i></span>
											</div>
											<div className="icon-content pt-4">
												<h3 style={{ fontSize: "1.5rem" }} id="addHeadingStyle" className=" dlab-title m-b15">{x.title1} <br /> {x.title2}</h3>
												<p className="m-b20" style={ParaStyle}>{x.desc}</p>
												{x.singleLink ? <Link href={x.singleLink}><a className="btn btn-outline-primary"><i className="fa fa-angle-right"></i></a></Link> : null}
											</div>
										</div>

									</a>
								</div>
							</Link>
						</div> */}
							</div>
								</Link>
						)}
					</Slider>
					{/* </div> */}

					{/* </div> */}
					{/* {link ? <Link href={link}><a style={{ display: "block", width: "max-content", margin: "auto" }} data-wow-delay="2s" data-wow-duration="3s" className="wow fadeInUp text-center  btn btn-corner gradient btn-primary">Know More</a></Link> : null} */}

				</div>
			</section>
		</>
	)
}

export default Service3;