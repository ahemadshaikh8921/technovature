import { CiLocationOn } from "react-icons/ci";
import { TfiEmail } from "react-icons/tfi";
import { IoCallOutline } from "react-icons/io5";
 
import Counterup from '../component/counterup';
function Location({ title, desc1, email,phone, img,reverse,locationName}) {
  return (
    <>
      {/* <!-- About Us --> */}
      <section className="content-inner" >
        <div className="container">
          <div className="row" style={reverse ? {flexDirection:"row-reverse"}:{flexDirection:"row"}}>
            {/* <div className="col-lg-6 m-b30">
              <div className="dz-media">
                <img src={img} className="move-2" alt="" />
              </div>
            </div> */}
            <div className="col-lg-12">
              <div className="section-head style-3 mb-4 d-flex align-items-center flex-column justify-content-center">
                <h2 className="title">{title}</h2>
                <div className="dlab-separator style-2 bg-primary"></div>
              </div>
              <h3 className="title text-center mt-4" style={{color: '#000', fontWeight:'500',fontFamily:'Outfit'}}>{locationName}</h3>
              <div className="row">
              <div className="col-12 d-flex align-items-start justify-content-around flex-wrap mt-4">
              <div className="loc_addr d-flex" >
              <i><CiLocationOn /></i> <p style={{width: '300px'}}>{desc1}</p>
              </div>
              <div className="loc_email d-flex align-items-center  justify-content-center" style={{width: '300px'}}>
              <i><TfiEmail /></i> <p><a href={`mailto:${email}`}>{email}</a></p>
              </div>
              <div className="loc_Phone d-flex" style={{width: '300px'}}>
              <i><IoCallOutline /></i> <p><a href={`tel:${phone}`}>{phone}</a></p>
              </div>
              </div>
              </div>

              <div className="d-flex align-items-center  justify-content-center mt-5">
                <img src={img} width={500} height={400} alt=""  />
              </div>


            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Location;
