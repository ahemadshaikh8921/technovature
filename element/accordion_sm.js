import { useState } from "react";


function Accordion_sm({ defaultAccordion }) {
    const [activeDefault, setActiveDefault] = useState(0);
    // const defaultAccordion = [
    //     {
    //         title: "Fusce sem ligula, imperdiet sed nisi sit amet ?",
    //         text:
    //             "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.",
    //         bg: "primary",
    //     },
    //     {
    //         title: "Maecenas aliquet quam sed tellus cursus ?",
    //         text:
    //             "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.",

    //         bg: "info",
    //     },
    //     {
    //         title: "Proin blandit sed arcu sed ultricies ?",
    //         text:
    //             "Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.",

    //         bg: "success",

    //     },
    //     {
    //         title: "Proin cursus massa ipsum, sit amet ?",
    //         text:
    //             "Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.",

    //         bg: "success",
    //     },
    //     {
    //         title: "Quisque sem tortor, accumsan finibus massa ?",
    //         text:
    //             "Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.",

    //         bg: "success",
    //     },

    // ];
    return (
        <>
            <div
                className="dlab-accordion accordion-sm"
                id="accordionFaq"
                defaultActiveKey="0"
            >
                {defaultAccordion && defaultAccordion.map((d, i) => (
                    <div className="card" key={i}>
                        <div
                            eventKey={`${i}`}
                            className="card-header"
                            onClick={() =>
                                setActiveDefault(activeDefault === i ? -1 : i)
                            }
                        >
                            <h5 className="dlab-title">
                                <a
                                    className={`${activeDefault === i ? "" : "collapsed"
                                        } faqMouse`}
                                    onClick={() =>
                                        setActiveDefault(
                                            activeDefault === i ? -1 : i
                                        )
                                    }
                                >
                                    {" "}
                                    {d.title}
                                </a>
                            </h5>
                        </div>
                        {/* <div className="collapse show" eventKey={`${i}`}>
                            <div className="card-body">
                                <p className="m-b0">{d.text}</p>
                            </div>
                        </div> */}
                        <div className={`${activeDefault === i ? "collapsed show" : "collapsed"
                            }`} eventKey={`${i}`} onClick={() =>
                                setActiveDefault(
                                    activeDefault === i ? -1 : i
                                )
                            }>
                            <div className="card-body">
                                {
                                    d.simple ? d.simple.map(d => {
                                        return <p className="m-b0 mt-3">{d}</p>
                                    }) :
                                        <p className="m-b0">{d.text}</p>
                                }
                                <ul>
                                    {d.text0UL && d.text0UL.map(d => {
                                        return <li id={d}>{d}</li>
                                    })}
                                    <ul>
                                        {
                                            d.text0UL_UL && d.text0UL_UL.map(d => {
                                                return <li id={d}>{d}</li>
                                            })
                                        }
                                    </ul>
                                </ul>
                                {
                                    d.A && d.A.map(d => {
                                        return <p style={{ margin: '0px' }}>{d}</p>
                                    })
                                }
                                <p className="m-b0 mt-3">{d.text1}</p>
                                {
                                    d.B && d.B.map(d => {
                                        return <p style={{ margin: '0px' }}>{d}</p>
                                    })
                                }
                                <ul>
                                    {d.text1UL && d.text1UL.map(d => {
                                        return <li id={d}>{d}</li>
                                    })}
                                    <ul>
                                        {
                                            d.text1UL_UL && d.text1UL_UL.map(d => {
                                                return <li id={d}>{d}</li>
                                            })
                                        }
                                    </ul>
                                </ul>
                                <p className="m-b0 mt-3">{d.text2}</p>
                                {
                                    d.C && d.C.map(d => {
                                        return <p style={{ margin: '0px' }}>{d}</p>
                                    })
                                }
                                <ul>
                                    {d.text2UL && d.text2UL.map(d => {
                                        return <li id={d}>{d}</li>
                                    })}
                                    <ul>
                                        {
                                            d.text2UL_UL && d.text2UL_UL.map(d => {
                                                return <li id={d}>{d}</li>
                                            })
                                        }
                                    </ul>
                                </ul>
                                
                                <p className="m-b0 mt-3">{d.text3}</p>
                                {
                                    d.D && d.D.map(d => {
                                        return <p style={{ margin: '0px' }}>{d}</p>
                                    })
                                }
                                <ul>
                                    {d.text3UL && d.text3UL.map(d => {
                                        return <li id={d}>{d}</li>
                                    })}
                                </ul>
                                <p className="m-b0 mt-3">{d.text4}</p>
                                {
                                    d.E && d.E.map(d => {
                                        return <p style={{ margin: '0px' }}>{d}</p>
                                    })
                                }
                                <p className="m-b0 mt-3">{d.text5}</p>
                                <p className="m-b0 mt-3">{d.text6}</p>
                                <p className="m-b0 mt-3">{d.text7}</p>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </>
    )
}

export default Accordion_sm;