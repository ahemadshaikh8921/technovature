import Image from "next/image";
import { LazyLoadImage } from "react-lazy-load-image-component";
import ContactForm from "./contact-form";
import Aos from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";
import ContactFormPage from "./contact-page-form";

function GetInTouch3({ test, contact }) {
    useEffect(() => {
        Aos.init({ duration: 2000 });
    }, [])
    return (
        <>
            {/* <!-- Contact Form --> */}
            <section data-aos="fade-up" className="content-inner " id="contact-now">
                <div className="container">
                    {test ? <div className="d-flex align-items-center justify-content-center">
                        <div className="col-sm-6" >
                            <button type="submit" className="btn btn-corner gradient btn-block btn-primary">Get In Touch With Us</button>
                        </div>
                    </div> :
                        <div className="row align-items-center flex-column justify-content-center">

                            {contact ?
                                <div className="section-head style-3 row">
                                    {/* <div className="d-flex align-items-center flex-column justify-content-center">
                                        <h2 className="title m-t10">Contact Us</h2>
                                        <div className="dlab-separator style-2 bg-primary"></div>
                                    </div> */}
                                    <div className="col-lg-6 m-b30 wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.4s">
                                        <div className="mt-4 mb-2 ml-4 d-flex flex-column">
                                            <h2 className="title m-t10">Get in touch !</h2>

                                            <p className="pt-2">Our friendly team would love to hear from you</p>
                                        </div>
                                        {contact ? <ContactFormPage /> : <ContactForm />}
                                    </div>

                                    {contact && <div className="col-lg-6 m-b30 wow fadeInLeft d-flex justify-content-end align-items-end" data-wow-duration="2s" data-wow-delay="0.2s">
                                        <div className="dlab-media d-flex justify-content-center align-items-center">
                                            <Image width={500} height={500} src="/newImg/contact/HomeContact.png" className="move-1" alt="" />
                                        </div>
                                    </div>}
                                </div>
                                : <div className="col-lg-6 section-head style-3">
                                    <div className="d-flex align-items-center flex-column justify-content-center">
                                        <h2 className="title m-t10">Contact Us</h2>
                                        <div className="dlab-separator style-2 bg-primary"></div>
                                    </div>
                                    <div className="mt-4 d-flex align-items-center flex-column justify-content-center">
                                        <h2 className="title m-t10 text-center">Want to create <br />
                                            something together?</h2>

                                        <p className="pt-2">Our friendly team would love to hear from you</p>
                                    </div>
                                </div>}
                            <div className="col-lg-5 m-b30 wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.4s">

                                {contact ? "" : <ContactForm />}
                            </div>


                        </div>}
                </div>
            </section>
        </>
    )
}

export default GetInTouch3;
