import Portfolio3 from "../component/portfolio-3";
import ServiceTemp from "../component/serviceTemp";

function Projects3({portfolio,desc,title,Heading, Data}) {
    return (
      <>
        {/* <!-- Projects --> */}
		<section className="content-inner pt-5">
			<div className="container">
				<div className="section-head style-3 text-center">
				{title?<h2 className="title">{title}</h2>:<h2 className="title">Our Portfolio</h2>}
					<div className="dlab-separator style-2 bg-primary"></div>
					<p>{desc}</p>
				</div>		
				{/* <Portfolio3 portfolio={portfolio} Data={Data} Heading={Heading}/> */}
				<ServiceTemp portfolio={portfolio} Data={Data} Heading={Heading}/>
			</div>
		</section>
      </>
    )
  }
  
  export default Projects3;