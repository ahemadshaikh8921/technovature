
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Link from "next/link";
import Wave from 'react-wavify'
import Counterup from '../component/counterup';
import FAQ from './faq';
import Accordion_sm from './accordion_sm';
import { redirect } from 'next/dist/next-server/server/api-utils';
import Accordion_f from './accordion_feature';
import { BsCheck2 } from "react-icons/bs";
import Image from 'next/image';
import Aos from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";


// images
// import HET_AI from "../public/images/Highly_experienced_team/HET_AI.png";
// import HET_AI2 from "../public/images/Highly_experienced_team/HET_AI_2.png";
// import HITAR_1 from "../public/images/HITAR/HITAR_1.png";
// import team_1 from "../public/images/wellteam/team_1.png";
// import EAP_1 from "../public/images/Eproactive/EAP_1.png";



function AboutUs3({ title, p100, Target, bggray, desc1, ID,changelayout, extraImages, feature, desc3, btn, link, desc2, counterData, img, style, sectionStyle, className, listData, featureClient }) {
  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, [])

  return (
    <>
      {/* <!-- About Us --> */}
      <section id={Target} className={"Feature-clients " + `${className}`} style={sectionStyle}>
        <div data-aos="fade-up" id={ID}  className={"container "+ `${p100}`}>
          {featureClient ? <div style={style} className="row">
            <div className="col-12 m-b30">
              {/* <div className="section-head d-flex align-items-center justify-content-center flex-column style-3 mb-4">
                <h2 className="title">{title}</h2>
                <div className="dlab-separator mt-3 style-2 bg-primary"></div>
              </div> */}
              {counterData ? <div className="row" style={{ marginTop: "40px" }}>
                {counterData.map((x, i) => {
                  return (
                    <div className="col-sm-4 m-b30 wow fadeInUp" data-wow-duration="2s" data-wow-delay={x.delay}>
                      <div className="dlab-content-bx style-2 text-center">
                        <div className="icon-content">
                          <h3 className="counter m-b0" style={{ color: "#002864 !important", marginBottom: "10px" }}><Counterup count={x.count} /><span>{x.sign}</span></h3>
                          <span className="title" style={{ lineHeight: "4px !important" }}>{x.title}</span>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div> : null}
            </div>
          </div> : 
          (
            changelayout ? 
            <div className="row" style={style}>
            <div className={img ? "col-lg-6" : "col-lg-12"}>
              {title ? <div className="section-head style-3 mb-4">
                <h2 className="title">{title}</h2>
                <div className="dlab-separator style-2 bg-primary"></div>
              </div> : null}

              {/* internal box */}
              {extraImages && <div className="row">
                {/* {
                  extraImages && extraImages.map(item => {
                    <div className="col-6">
                      <div className="dlab-content-bx style-2 text-center">
                        <LazyLoadImage src={item.img} className="" alt="" />
                      </div>
                    </div>
                  })
                } */}
                <div className="col-6">
                  <div style={{ width: '60%', margin: '0 auto' }} >
                    <div className="dlab-content-bx style-2 text-center">
                      <img src={extraImages && extraImages[0].img} alt="Highly experienced team" />
                    </div>
                  </div>
                  {/* <p className='text-center'>Highly experienced team</p> */}
                  <p style={{ fontSize: '1.02rem' }} class="dlab-position text-center pt-2">Highly experienced team</p>
                </div>
                <div className="col-6">
                  <div style={{ width: '60%', margin: '0 auto' }} >
                    <div className="dlab-content-bx style-2 text-center">
                      <img src={extraImages && extraImages[1].img} alt="Highly experienced team" />
                    </div>
                  </div>
                  {/* <p className='text-center'>Highly experienced team</p> */}
                  <p style={{ fontSize: '1.02rem' }} class="dlab-position text-center pt-2">Highly experienced team</p>
                </div>

                <div className="col-6 mt-2">
                  <div style={{ width: '60%', margin: '0 auto' }} >
                    <div className="dlab-content-bx style-2 text-center">
                      <img src={extraImages && extraImages[2].img} alt="Highly experienced team" />
                    </div>
                  </div>
                  {/* <p className='text-center'>Highly experienced team</p> */}
                  <p style={{ fontSize: '1.02rem' }} class="dlab-position text-center pt-2">Highly experienced team</p>
                </div>
                <div className="col-6 mt-2">
                  <div style={{ width: '60%', margin: '0 auto' }} >
                    <div className="dlab-content-bx style-2 text-center">
                      <img src={extraImages && extraImages[3].img} alt="Highly experienced team" />
                    </div>
                  </div>
                  {/* <p className='text-center'>Highly experienced team</p> */}
                  <p style={{ fontSize: '1.02rem' }} class="dlab-position text-center pt-2">Highly experienced team</p>
                </div>

              </div>}


              {desc1 && <p className='descText'>
                {desc1}
              </p>}
              {desc2 && <p className="m-b20 descText">
                {desc3 && <BsCheck2 />} {desc2}
              </p>}
              {desc3 && <p className="m-b20 descText">
                <BsCheck2 /> {desc3}
              </p>}

              {/* {link?<FAQ link />:null} */}
              {link ? <Link href={link}><a style={{ display: "block", width: "max-content" }} data-wow-delay="2s" data-wow-duration="3s" className="wow fadeInUp text-center  btn btn-corner gradient btn-primary sameDesignBtn">Know More</a></Link> : null}
              {/* {link?<Accordion_f feature={feature} />:null} */}
              {btn ? <div className="text-center"><button type="submit" className="btn btn-corner gradient  btn-primary">{btn}</button></div> : null}
              {/* {counterData ? <div className="row" style={{ marginTop: "40px" }}>
                {counterData.map((x, i) => {
                  return (
                    <div className="col-sm-4 m-b30 wow fadeInUp" data-wow-duration="2s" data-wow-delay={x.delay}>
                      <div className="dlab-content-bx style-2 text-center">
                        <div className="icon-content">
                          <h3 className="counter m-b0" style={{ color: "#002864 !important", marginBottom: "10px" }}><Counterup count={x.count} /><span>{x.sign}</span></h3>
                          <span className="title" style={{ lineHeight: "4px !important" }}>{x.title}</span>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div> : null} */}
              {listData ? <div className="row">

                <ul style={{ marginLeft: "22px" }}>
                  {listData.map((x, i) => {
                    return (
                      <li style={{ margin: "15px 0" }} key={x.content + i}><p><BsCheck2 style={{ marginRight: "10px" }} />{x.content}</p></li>
                    )
                  })}
                </ul>
              </div> : null}
            </div>
            <div className="col-lg-6 m-b30">
              <div className="dz-media d-flex align-items-center" style={{justifyContent:'end'}}>
                {ID ? <img  src={img} className="move-2" alt="" /> :<Image width={500} height={400} src={img} className="move-2" alt="" />}
              </div>
            </div>

          </div>
            :
          <div className="row" style={style}>
            <div className="col-lg-6 m-b30">
              <div className="d-flex align-items-center justify-content-end">
                {ID ? <img src={img} className="move-2" alt="" /> :<Image width={500} height={listData ? 500 : 400} src={img} className="move-2" alt="" />}
              </div>
            </div>
            <div className={img ? "col-lg-6" : "col-lg-12"}>
              {title ? <div className="section-head style-3 mb-4">
                <h2 className="title">{title}</h2>
                <div className="dlab-separator style-2 bg-primary"></div>
              </div> : null}

              {/* internal box */}
              {extraImages && <div className="row">
                {/* {
                  extraImages && extraImages.map(item => {
                    <div className="col-6">
                      <div className="dlab-content-bx style-2 text-center">
                        <LazyLoadImage src={item.img} className="" alt="" />
                      </div>
                    </div>
                  })
                } */}
                <div className="col-6">
                  <div style={{ width: '60%', margin: '0 auto' }} >
                    <div className="dlab-content-bx style-2 text-center">
                      <img src={extraImages && extraImages[0].img} alt="Highly experienced team" />
                    </div>
                  </div>
                  {/* <p className='text-center'>Highly experienced team</p> */}
                  <p style={{ fontSize: '1.02rem' }} class="dlab-position text-center pt-2">Highly experienced team</p>
                </div>
                <div className="col-6">
                  <div style={{ width: '60%', margin: '0 auto' }} >
                    <div className="dlab-content-bx style-2 text-center">
                      <img src={extraImages && extraImages[1].img} alt="Highly experienced team" />
                    </div>
                  </div>
                  {/* <p className='text-center'>Highly experienced team</p> */}
                  <p style={{ fontSize: '1.02rem' }} class="dlab-position text-center pt-2">Highly experienced team</p>
                </div>

                <div className="col-6 mt-2">
                  <div style={{ width: '60%', margin: '0 auto' }} >
                    <div className="dlab-content-bx style-2 text-center">
                      <img src={extraImages && extraImages[2].img} alt="Highly experienced team" />
                    </div>
                  </div>
                  {/* <p className='text-center'>Highly experienced team</p> */}
                  <p style={{ fontSize: '1.02rem' }} class="dlab-position text-center pt-2">Highly experienced team</p>
                </div>
                <div className="col-6 mt-2">
                  <div style={{ width: '60%', margin: '0 auto' }} >
                    <div className="dlab-content-bx style-2 text-center">
                      <img src={extraImages && extraImages[3].img} alt="Highly experienced team" />
                    </div>
                  </div>
                  {/* <p className='text-center'>Highly experienced team</p> */}
                  <p style={{ fontSize: '1.02rem' }} class="dlab-position text-center pt-2">Highly experienced team</p>
                </div>

              </div>}


              {desc1 && <p className='descText'>
                {desc1}
              </p>}
              {desc2 && <p className="m-b20 descText">
                {desc3 && <BsCheck2 />} {desc2}
              </p>}
              {desc3 && <p className="m-b20 descText">
                <BsCheck2 /> {desc3}
              </p>}

              {/* {link?<FAQ link />:null} */}
              {link ? <Link href={link}><a style={{ display: "block", width: "max-content" }} data-wow-delay="2s" data-wow-duration="3s" className="wow fadeInUp text-center  btn btn-corner gradient btn-primary sameDesignBtn">Know More</a></Link> : null}
              {/* {link?<Accordion_f feature={feature} />:null} */}
              {btn ? <div className="text-center"><button type="submit" className="btn btn-corner gradient  btn-primary">{btn}</button></div> : null}
              {/* {counterData ? <div className="row" style={{ marginTop: "40px" }}>
                {counterData.map((x, i) => {
                  return (
                    <div className="col-sm-4 m-b30 wow fadeInUp" data-wow-duration="2s" data-wow-delay={x.delay}>
                      <div className="dlab-content-bx style-2 text-center">
                        <div className="icon-content">
                          <h3 className="counter m-b0" style={{ color: "#002864 !important", marginBottom: "10px" }}><Counterup count={x.count} /><span>{x.sign}</span></h3>
                          <span className="title" style={{ lineHeight: "4px !important" }}>{x.title}</span>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div> : null} */}
              {listData ? <div className="row">

                <ul style={{ marginLeft: "22px" }}>
                  {listData.map((x, i) => {
                    return (
                      <li style={{ margin: "15px 0" }} key={x.content + i}><p><BsCheck2 style={{ marginRight: "10px" }} />{x.content}</p></li>
                    )
                  })}
                </ul>
              </div> : null}
            </div>

          </div>
          )
          }

          {/* {link ? <Accordion_f feature={feature} /> : null} */}

          {/* counters */}
          {counterData ? 
          <div className="row">
          <div id="counterOuter" className='col-12'>
          <div id="counterOuter_inner">
            {counterData.map((x, i) => {
              return (
                <div style={{ zIndex: '10' }} className="col-sm-4 m-b30 wow fadeInUp" data-wow-duration="2s" data-wow-delay={x.delay}>
                  <div className="dlab-content-bx style-2 text-center">
                    <div className="icon-content d-flex align-items-center flex-wrap" style={{ justifyContent: 'space-evenly' }}>
                      <h3 className="counter m-b0"><Counterup count={x.count} /><span>{x.sign}</span></h3>
                      <span className="title" id="titleOfCounter" style={{ lineHeight: "4px", fontWeight: '400', color: 'black', fontSize: '18px' }}>{x.title}</span>
                    </div>
                  </div>
                </div>
              )
            })}
            {/* <div id="waveWhite">
              <Wave fill='#fff'
                paused={false}
                options={{
                  height: 20,
                  amplitude: 40,
                  speed: 0.20,
                  points: 3
                }}
              />
            </div> */}
            </div>
            </div>
          </div> : null}

        </div>
      </section>
    </>
  );
}

export default AboutUs3;