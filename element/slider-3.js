import Image from "next/image";
import Link from "next/link";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Aos from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";

function Slider3 ({subtitle,title,desc, desc1, desc2,link,form,contact}) {
	useEffect(()=> {
		Aos.init({duration: 2000});
	}, [])
    return (
      <>
        
		<div className="banner-three bg-white">
		{/* style={{"backgroundImage":"url(images/background/bg5.png), url(images/background/bg6.png), var(--gradient-sec);",height:"max-content"}} */}
			<div data-aos="fade-up" className="container fadeInUp">
				<div className="banner-inner">
					<div className="row align-items-center">
						<div className="col-md-6">
							<div className="banner-content text-white">
								<div data-wow-delay="0.5s" data-wow-duration="3s" className="wow fadeInUp sub-title">{subtitle}</div>
								<h1 data-wow-delay="1s" data-wow-duration="3s" className="wow fadeInUp m-b20">{title}</h1>
								<p  data-wow-delay="1.5s" data-wow-duration="3s" className="wow fadeInUp m-b30 descText">{desc}</p>
								{(desc1 ? <p   data-wow-delay="1.5s" data-wow-duration="3s" className="wow descText fadeInUp m-b30">{desc1}</p> : null)}
								{(desc2 ? <p   data-wow-delay="1.5s" data-wow-duration="3s" className="wow descText fadeInUp m-b30">{desc2}</p> : null)}
								{/* <div className="mt-4">
								<Link href={link}><a data-wow-delay="2s" data-wow-duration="3s" className="wow fadeInUp sameDesignBtn btn btn-corner gradient btn-primary">Contact Now</a></Link>
								</div> */}
								
							</div>
						</div>
						<div className="col-md-6">
							{form?contact:<div className="dz-media wow fadeIn" data-wow-delay="1s" data-wow-duration="3s">
								<Image width={500} height={450} src="/newImg/home/banner-1.png" className="move-1" alt=""/>
							</div>}
						</div>
					</div>
				</div>
			</div>
		</div>
      </>
    )
  }
  
  export default Slider3;