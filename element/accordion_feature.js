import { useState } from "react";


function Accordion_f({feature}) {
    const [activeDefault, setActiveDefault] = useState(1);

    // const defaultAccordion = [];
    return (
        <>
            <div style={{marginTop:"30px"}}
                className="dlab-accordion accordion-sm"
                id="accordionFaq"
                defaultActiveKey="0"
            >
                {/* {feature1.map((d, i) => ( */}
                    <div className="card" >
                        <div
                            
                            className="card-header"
                            onClick={() =>
                                setActiveDefault(activeDefault === 0 ? -1 : 0)
                            }
                        >
                            <h5 className="dlab-title">
                                <a
                                    className={`${activeDefault === 0 ? "" : "collapsed"
                                        } faqMouse`}
                                    onClick={() =>
                                        setActiveDefault(
                                            activeDefault === 0 ? -1 : 0
                                        )
                                    }
                                >
                                    {" "}
                                    {"Features"}
                                </a>
                            </h5>
                        </div>
                        {/* <div className="collapse show" eventKey={`${i}`}>
                            <div className="card-body">
                                <p className="m-b0">{d.text}</p>
                            </div>
                        </div> */}
                        <div className={`${activeDefault === 0 ? "collapsed show" : "collapsed"
                            }`}  onClick={() =>
                                setActiveDefault(
                                    activeDefault === 0 ? -1 : 0
                                )
                            }>
                            <div className="card-body">
                            {feature.map((d,i) => {
                                return <><p key={i} className="m-b1">- {d}</p> </>
                            })}
                            </div>
                        </div>
                    </div>
                {/* ))} */}
            </div>
        </>
    )
}

export default Accordion_f;