import React from "react";
import { Helmet } from "react-helmet";
import Head from "next/head"

function Seo({title,desc,canonical}) {
    return (
        <>
            <Head>
                <meta charSet="utf-8" />
                <title>{title}</title>
                <link rel="canonical" href={canonical} />
                <meta name="description" content={desc} />
            </Head>
        </>
    );
}

export default Seo;