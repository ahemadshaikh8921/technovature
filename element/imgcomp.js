import Link from 'next/link';

function ImgComp({ src }) {

    return (
        <>
            <div style={{display:"block",margin:"20px auto"}} className="dlab-media m-b30 rounded-md">
                <img src={src} alt="" />
            </div>
        </>
    )
}

export default ImgComp;