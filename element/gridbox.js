import Link from 'next/link';

function GridBox({ title, desc, icon, delay, }) {

    return (
        <>
            {/* <!-- Service --> */}
            
                <div  className="col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay={delay}>
                    <div className={"dlab-blog style-1  icon-bx-wraper style-5 box-hover text-center m-b30"} >
                        <div className="icon-bx">
                            <span className="icon-cell"><i><img style={{ width: "45%" }} src={icon} /></i></span>
                        </div>
                        <div className="icon-content">
                            <h3 style={{fontSize:"1.5rem"}} className="dlab-title m-b15">{title}</h3>
                            <p className="m-b20">{desc}</p>
                        </div>
                    </div>
                </div>
        </>
    )
}

export default GridBox;