import Accordion_sm from "../element/accordion_sm";
import Footer3 from "../layout/footer-3";
import Header3 from "../layout/header-3";
import Link from 'next/link';


function FAQ({defaultAccordion}) {
  return (
    <>
      <div className="page-content ">
		{/* <!-- Faq --> */}
		<section className="content-inner bg-gray">
			<div className="container">
				<div className="row align-items-center">
					<div className="col-12 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
					<div className="section-head style-3 text-center mb-4">
						<h2 className="title">FAQ</h2>
						<div className="dlab-separator style-2 bg-primary"></div>
					</div>
						<Accordion_sm defaultAccordion={defaultAccordion} />
					</div>
				</div>
			</div>
		</section>
			
	</div>
    </>
  )
}

export default FAQ;
