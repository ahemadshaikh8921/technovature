import Link from 'next/link';

import { LazyLoadImage } from 'react-lazy-load-image-component';
import Counterup from '../component/counterup';
function Connect_with_us({ title, bggray, desc1, btn, desc2, counterData, img, style, sectionStyle, className, listData, featureClient }) {
  return (
    <>
      {/* <!-- About Us --> */}
      <section className={"connectwithus Feature-clients " + `${className}` + ` ${bggray}`} style={sectionStyle}>
        <div className="container">
          <div style={style} className="row">
            <div className="row" style={style}>
              <div className="col-lg-12 text-center">
                <p>
                  {desc1}
                </p>
                <p className="m-b20">
                  {desc2}
                </p>
                {/* <div className="text-center"><button type="submit" className="btn btn-corner gradient  btn-primary"></button></div> */}
                <div className="col-sm-12 text-center" >
                  {/* <button type="submit" style={{ width: 'auto',textTransform:'capitalize' }} className="sameDesignBtn btn btn-corner gradient btn-block btn-primary"></button> */}
                  <Link href="/contact"><a id='navbarContactBTN' className='sameDesignBtn btn btn-corner gradient btn-primary'>{btn}</a></Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Connect_with_us;
