import React from 'react'

const FeaturesComponents = ({title, desc1, desc2}) => {
    return (
        <>
            <div className="section-head style-3 mb-4">
                <h2 className="feature-subtitle">{title}</h2>
                {title?<div className="dlab-separator style-2 bg-primary"></div>:null}
            </div>
            <p style={{marginTop: '-15px'}} >
                {desc1}
            </p>
            <p className="m-b20">
                {desc2}
            </p>
        </>
    )
}

export default FeaturesComponents