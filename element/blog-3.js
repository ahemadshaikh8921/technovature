import Link from "next/link"

function Blog3({ BlogData ,children, title, bggray}) {
	return (
		<>
			{/* <!-- Blog --> */}
			<section className={"content-inner "+`${bggray}`}>
				<div className="container">
					<div className="section-head style-3 text-center">
						<h2 className="title">{title? 'Featured Blog Posts':`Knowledge Hub`}</h2>
						<div className="dlab-separator style-2 bg-primary"></div>
						<p>Plunge to the world of knowledge with us.</p>
					</div>
					<div className="row">
						{children}
						
						{/* <div className="col-xl-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
						<div className="dlab-blog blog-half m-b30">
							<div className="dlab-media">
								<img src="images/blog/default/thum1.jpg" alt=""/>
							</div>
							<div className="dlab-info">
								<h5 className="dlab-title">
									<Link href="/blog-details-3"><a>Modern JamStack Websites</a></Link>
								</h5>
								<p>New to JAMstack? Everything You Need to Know to Get Started When we first wrote this post about JAMStack, the JAMstack ecosystem was still…</p>
								<div className="dlab-meta">
									<ul>
										<li className="post-date"><i className="flaticon-clock m-r10"></i>7 March, 2020</li>
										<li className="post-comment"><a href="#"><i className="flaticon-speech-bubble"></i><span>15</span></a></li>
										<li className="post-share"><i className="flaticon-share"></i>
											<ul>
												<li><a className="fa fa-facebook" href="#"></a></li>
												<li><a className="fa fa-twitter" href="#"></a></li>
												<li><a className="fa fa-linkedin" href="#"></a></li>
											</ul>
										</li>
									</ul>
								</div>								
							</div>
						</div>
					</div>
					<div className="col-xl-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.4s">
						<div className="dlab-blog blog-half m-b30">
							<div className="dlab-media">
                                <img src="images/blog/default/thum2.jpg" alt=""/>
							</div>
							<div className="dlab-info">
								<h5 className="dlab-title">
									<Link href="/blog-details-3"><a>The Great Gatsby</a></Link>
								</h5>
								<p>So What is GatsbyJS? GatsbyJS is a modern website framework with built-in performance to allow businesses to easily create ultra-fast sites…</p>
								<div className="dlab-meta">
									<ul>
										<li className="post-date"><i className="flaticon-clock m-r10"></i>7 March, 2020</li>
										<li className="post-comment"><a href="#"><i className="flaticon-speech-bubble"></i><span>15</span></a></li>
										<li className="post-share"><i className="flaticon-share"></i>
											<ul>
												<li><a className="fa fa-facebook" href="#"></a></li>
												<li><a className="fa fa-twitter" href="#"></a></li>
												<li><a className="fa fa-linkedin" href="#"></a></li>
											</ul>
										</li>
									</ul>
								</div>								
							</div>
						</div>
					</div>
					<div className="col-xl-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
						<div className="dlab-blog blog-half m-b30">
							<div className="dlab-media">
								<img src="images/blog/default/thum3.jpg" alt=""/>
							</div>
							<div className="dlab-info">
								<h5 className="dlab-title">
									<Link href="/blog-details-3"><a>Headless CMS</a></Link>
								</h5>
								<p>7 Reasons to Consider a Headless CMS There are many reasons for a business or an organization to consider a headless content management…</p>
								<div className="dlab-meta">
									<ul>
										<li className="post-date"><i className="flaticon-clock m-r10"></i>7 March, 2020</li>
										<li className="post-comment"><a href="#"><i className="flaticon-speech-bubble"></i><span>15</span></a></li>
										<li className="post-share"><i className="flaticon-share"></i>
											<ul>
												<li><a className="fa fa-facebook" href="#"></a></li>
												<li><a className="fa fa-twitter" href="#"></a></li>
												<li><a className="fa fa-linkedin" href="#"></a></li>
											</ul>
										</li>
									</ul>
								</div>								
							</div>
						</div>
					</div>
					<div className="col-xl-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.8s">
						<div className="dlab-blog blog-half m-b30">
							<div className="dlab-media">
								<img src="images/blog/default/thum4.jpg" alt=""/>
							</div>
							<div className="dlab-info">
								<h5 className="dlab-title">
									<Link href="/blog-details-3"><a>AI Chatbots</a></Link>
								</h5>
								<p>AI Conversational Interfaces The first conceptualization of the chatbot is attributed to Alan Turing, who asked “Can machines think?” in…</p>
								<div className="dlab-meta">
									<ul>
										<li className="post-date"><i className="flaticon-clock m-r10"></i>7 March, 2020</li>
										<li className="post-comment"><a href="#"><i className="flaticon-speech-bubble"></i><span>15</span></a></li>
										<li className="post-share"><i className="flaticon-share"></i>
											<ul>
												<li><a className="fa fa-facebook" href="#"></a></li>
												<li><a className="fa fa-twitter" href="#"></a></li>
												<li><a className="fa fa-linkedin" href="#"></a></li>
											</ul>
										</li>
									</ul>
								</div>								
							</div>
						</div>
					</div> */}
					</div>
					<Link href={"/blog"}><a style={{display:"block",width:"max-content",margin:"auto"}}  data-wow-delay="2s" data-wow-duration="3s" className="wow fadeInUp text-center  btn btn-corner gradient btn-primary">Read More</a></Link> 
				
				</div>
			</section>
		</>
	)
}

export default Blog3;