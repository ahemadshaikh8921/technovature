import React from 'react'
import { AI_Feature1, AI_Feature2, AI_Feature3, AI_Feature4 } from '../content/home'
import FeaturesComponents from './featuresComponents'

const features = ({ title,bggray,children, style, sectionStyle, className}) => {

  return (
    <>
      {/* <!-- About Us --> */}
      <section id='features_services' className={" Feature-clients " + `${className}` + ` ${bggray}` } style={sectionStyle}>
        <div className="container">
        <div className="section-head style-3 text-center mb-4">
						<h2 className="title">{title}</h2>
						<div className="dlab-separator style-2 bg-primary"></div>
					</div>
          <div className="row" style={style}>
          {children}
          </div>
        </div>
      </section>
    </>
  )
}

export default features