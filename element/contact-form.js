function ContactForm() {
    return (
        <>
            <form className="dlab-form style-1 dzForm" method="POST" action="https://getform.io/f/65d479d0-54d5-45ac-a2e1-1e54d851084b">
                
                <div className="dzFormMsg"></div>
                <input type="hidden" className="form-control" name="ContactForm" value="Contact" />
                <div className="row">
                    <div className="col-sm-12">
                        <div className="input-group">
                            <input name="Name" type="text" required className="form-control" placeholder="Full Name" />
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className="input-group">
                            <input name="Email" type="text" required className="form-control" placeholder="Email Address" />
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className="input-group">
                            <input name="Phone" type="text" required className="form-control" placeholder="Phone No." />
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className="input-group">
                            <textarea name="Message" className="form-control" required placeholder="Please describe in brief what you would like to communicate..."></textarea>
                        </div>
                    </div>
                    <div className="col-sm-12 text-center" >
                        {/* <Link href={link}><a data-wow-delay="2s" data-wow-duration="3s" className="wow fadeInUp sameDesignBtn">Contact Now</a></Link> */}
                        <button type="submit" style={{ width: 'auto' }} className="sameDesignBtn btn btn-corner gradient btn-block btn-primary">Submit Now</button>
                    </div>
                </div>
            </form>
        </>
    )
}

export default ContactForm;