
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Aos from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";
import Wave from 'react-wavify'
function Team3({ TeamData, title }) {
	useEffect(() => {
		Aos.init({ duration: 2000 });
	}, [])
	return (
		<>
			<section className="content-inner">
				<div data-aos="fade-up" className="container">
					<div className="section-head style-3 text-center mb-4">
						<h2 className="title">{title ? "Meet Our Team" : "Our Professionals"}</h2>
						<div className="dlab-separator style-2 bg-primary"></div>
					</div>
					<div className="row">
						<div id="fullWidthTeam">
							<div className="col-lg-12">
								{/* <TeamSlider3 {...TeamData}/> */}
								<div className="row" id='teamBoxA' >
									{/* <div id='teamBoxA'> */}
									{TeamData.map((x, i) =>
										<div key={x.name + i} className="col-lg-4 col-12 item wow fadeInUp" style={{ background: 'white', borderRadius: '30px', width: '100%', height: '200px', marginTop: '100px', zIndex: '1' }} data-wow-duration="2s" data-wow-delay={x.delay}>
											<div style={{ width: '100%', height: '135%', transform: 'translateY(-70px)' }} className="dlab-team style-2 m-b10  d-flex align-items-center justify-content-center flex-column">
												<div style={{ width: '150px', height: 'auto', borderRadius: '50%', border: '5px solid #5278FF' }} className="dlab-media">
													<img src={x.img} alt="" />
												</div>
												<div className="dlab-content" style={{ paddingBottom: 0 }}>
													<div className="clearfix">
														<h3 className="dlab-name">{x.name}</h3>
														<p style={{ fontSize: "1.02rem" }} className="dlab-position">{x.position}</p>
													</div>
												</div>
											</div>
										</div>)}
									{/* </div> */}
									{/* <div id="waveWhite2">
										<Wave fill='#fff'
											paused={false}
											options={{
												height: 20,
												amplitude: 40,
												speed: 0.20,
												points: 3
											}}
										/> </div> */}
								</div>
							</div>

							{/* background animation wave line */}
							{/* <div className="bg_wave_line">
							</div> */}
						</div>
					</div>
				</div>
			</section>
		</>)
}

export default Team3;

