import { useEffect, useState } from "react";
import SimpleReactLightbox from 'simple-react-lightbox';
import { SRLWrapper, useLightbox } from 'simple-react-lightbox';
import AboutUs3 from "../element/aboutUs-3";
import Aos from "aos";
import "aos/dist/aos.css";

function Portfolio3({ portfolio, Heading, Data }) {
  // console.log(Heading, "heading")
  const [filter, setFilter] = Heading ? useState("blockchain") : useState("all");
  const [projects, setProjects] = useState([]);
  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, [])

  useEffect(() => {
    if (Heading) {
      setProjects(Data);
    } else
      setProjects(portfolio);
  }, []);

  console.log(projects, "projects")

  useEffect(() => {
    setProjects([]);
    if (Heading) {
      const filtered = Data.map(d => ({
        ...d,
        filtered: d.category.includes(filter),
      }))
      setProjects(filtered);
    } else {
      const filtered = portfolio.map((p) => ({
        ...p,
        filtered: p.category.includes(filter),
      }))
      setProjects(filtered);
    }
  }, [filter]);
  return (
    <>
      <div data-aos="fade-up" className="site-filters style-3 clearfix center m-b40">
        {Heading ?
          <ul className="filters">
            <div className="row">
              <div className="col-12">
                <li className={`btn ${filter === `blockchain` ? "active" : ""}`}>
                  <a active='blockchain' onClick={() => setFilter("blockchain")}>Blockchain</a>
                </li>

                <li className={`btn ${filter === `cloud` ? "active" : ""}`}>
                  <a active='cloud' onClick={() => setFilter("cloud")}>Cloud Computing</a>
                </li>

                <li className={`btn ${filter === `bigdata` ? "active" : ""}`}>
                  <a active='bigdata' onClick={() => setFilter("bigdata")}>Big Data</a>
                </li>
              </div>

              <div className="col-12 mt-3">

                <li className={`btn ${filter === `iot` ? "active" : ""}`}>
                  <a active='iot' onClick={() => setFilter("iot")}>Internet of Things</a>
                </li>

                <li className={`btn ${filter === `ai` ? "active" : ""}`}>
                  <a active='ai' onClick={() => setFilter("ai")}>Artificial intelligence</a>
                </li>

                <li className={`btn ${filter === `webdev` ? "active" : ""}`}>
                  <a active='webdev' onClick={() => setFilter("webdev")}>Web development</a>
                </li>
              </div>

              {/* {Heading.map((head, i) => {
            return <li className={`btn ${filter === `${head.btn_s}` ? "active" : ""}`}>
              <a active={head.btn_s} onClick={() => setFilter(head.btn_s)}>{head.btn_l}</a>
            </li>
          })} */}
            </div>

          </ul> :
          <ul className="filters">
            <li className={`btn ${filter === "all" ? "active" : ""}`}>
              <a active={"all"} onClick={() => setFilter("all")}>All</a>
            </li>
            <li className={`btn ${filter === "cloud" ? "active" : ""}`}>
              <a active={"cloud"} onClick={() => setFilter("cloud")} > Cloud</a>
            </li>
            <li className={`btn ${filter === "ai" ? "active" : ""}`}>
              <a active={"ai"} onClick={() => setFilter("ai")}>AI </a>
            </li>
            <li className={`btn ${filter === "blockchain" ? "active" : ""}`}>
              <a active={"blockchain"} onClick={() => setFilter("blockchain")} >  Blockchain</a>
            </li>
            <li className={`btn ${filter === "banking-fintech" ? "active" : ""}`}>
              <a active={"banking-fintech"} onClick={() => setFilter("banking-fintech")}> Banking & fintech</a>
            </li>
            <li className={`btn ${filter === "mobile-5g" ? "active" : ""}`}>
              <a active={"mobile-5g"} onClick={() => setFilter("mobile-5g")}>  Mobile and 5G </a>
            </li>
            <li className={`btn ${filter === "others" ? "active" : ""}`}>
              <a active={"others"} onClick={() => setFilter("others")}> Others </a>
            </li>
          </ul>}
      </div>
      <SimpleReactLightbox>
        <SRLWrapper >
          <div className="clearfix">
            {Heading ?
              <ul id="masonry" className="row" data-column-width="12">
                {projects.map((item) =>
                  item.filtered === true ? (
                    <AboutUs3 {...item} />
                  ) : null
                )}
              </ul>
              :
              <ul id="masonry" className="row" data-column-width="3">
                {projects.map((item) =>
                  item.filtered === true ? (
                    <>
                      <li className="card-container col-lg-4 col-md-6 col-sm-6 cloud wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.1s">
                        <div className="dlab-box dlab-blog style-1 style-3 m-b30" style={{ background: "white" }}>
                          <div className="dlab-media ">{item.img}</div>
                          <div className="dlab-info">
                            <h3 style={{ fontSize: "1.5rem", margin: "10px 0" }} className="title"> {item.title} </h3>
                            <p className="post-author"> {item.desc} </p>
                          </div>
                        </div>
                      </li>
                    </>) : ("")
                )}
              </ul>
            }
          </div>
        </SRLWrapper >
      </SimpleReactLightbox>
    </>
  );
}
export default Portfolio3;

