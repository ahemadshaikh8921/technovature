import { useEffect, useState } from "react";
import SimpleReactLightbox from 'simple-react-lightbox';
import { SRLWrapper, useLightbox } from 'simple-react-lightbox';
import AboutUs3 from "../element/aboutUs-3";
import Aos from "aos";
import "aos/dist/aos.css";
import Link from "next/link";

function ServiceTemp({ portfolio, Heading, Data }) {
  // console.log(Heading, "heading")
  const [filter, setFilter] = Heading ? useState("blockchain") : useState("all");
  const [projects, setProjects] = useState([]);
  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, [])

  useEffect(() => {
    if (Heading) {
      setProjects(Data);
    } else
      setProjects(portfolio);
  }, []);

  console.log(projects, "projects")

  useEffect(() => {
    setProjects([]);
    if (Heading) {
      const filtered = Data.map(d => ({
        ...d,
        filtered: d.category.includes(filter),
      }))
      setProjects(filtered);
    } else {
      const filtered = portfolio.map((p) => ({
        ...p,
        filtered: p.category.includes(filter),
      }))
      setProjects(filtered);
    }
  }, [filter]);
  return (
    <>
      <div data-aos="fade-up" className="site-filters style-3 clearfix center m-b40">
        {Heading ?
          <ul id="btn_pages" className="filters">
            <div className="row">
              <div className="col-12">
                <li className={`btn ${filter === `blockchain` ? "active" : ""}`}>
                <Link href="/services/temp/#blockchain" >
                  <a active='blockchain' onClick={() => setFilter("blockchain")}>Blockchain</a>
                  </Link>
                </li>

                <li className={`btn ${filter === `cloud` ? "active" : ""}`}>
                <Link href="/services/temp/#cloud_computing" >
                  <a  active='cloud' onClick={() => setFilter("cloud")}>Cloud Computing</a></Link>
                </li>

                <li className={`btn ${filter === `bigdata` ? "active" : ""}`}>
                <Link href="/services/temp/#bigdata" >
                  <a active='bigdata' onClick={() => setFilter("bigdata")}>Big Data</a>
                  </Link>
                </li>
              </div>

              <div className="col-12 mt-3">

                <li className={`btn ${filter === `iot` ? "active" : ""}`}>
                <Link href="/services/temp/#iot" >
                  <a active='iot' onClick={() => setFilter("iot")}>Internet of Things</a>
                  </Link>
                </li>

                <li className={`btn ${filter === `ai` ? "active" : ""}`}>
                <Link href="/services/temp/#autificial_intelligence" >
                  <a active='ai' onClick={() => setFilter("ai")}>Artificial intelligence</a>
                  </Link>
                </li>

                <li className={`btn ${filter === `webdev` ? "active" : ""}`}>
                <Link href="/services/temp/#web_development" >
                  <a active='webdev' onClick={() => setFilter("webdev")}>Web development</a>
                  </Link>
                </li>
              </div>

              {/* {Heading.map((head, i) => {
            return <li className={`btn ${filter === `${head.btn_s}` ? "active" : ""}`}>
              <a active={head.btn_s} onClick={() => setFilter(head.btn_s)}>{head.btn_l}</a>
            </li>
          })} */}
            </div>

          </ul> :
          <ul className="filters">
            <li className={`btn ${filter === "all" ? "active" : ""}`}>
              <a active={"all"} onClick={() => setFilter("all")}>All</a>
            </li>
            <li className={`btn ${filter === "cloud" ? "active" : ""}`}>
              <a active={"cloud"} onClick={() => setFilter("cloud")} > Cloud</a>
            </li>
            <li className={`btn ${filter === "ai" ? "active" : ""}`}>
              <a active={"ai"} onClick={() => setFilter("ai")}>AI </a>
            </li>
            <li className={`btn ${filter === "blockchain" ? "active" : ""}`}>
              <a active={"blockchain"} onClick={() => setFilter("blockchain")} >  Blockchain</a>
            </li>
            <li className={`btn ${filter === "banking-fintech" ? "active" : ""}`}>
              <a active={"banking-fintech"} onClick={() => setFilter("banking-fintech")}> Banking & fintech</a>
            </li>
            <li className={`btn ${filter === "mobile-5g" ? "active" : ""}`}>
              <a active={"mobile-5g"} onClick={() => setFilter("mobile-5g")}>  Mobile and 5G </a>
            </li>
            <li className={`btn ${filter === "others" ? "active" : ""}`}>
              <a active={"others"} onClick={() => setFilter("others")}> Others </a>
            </li>
          </ul>}
      </div>
      <SimpleReactLightbox>
        <SRLWrapper >
          <div className="clearfix">
              <ul id="masonry" className="row" data-column-width="12">
                {projects.map((item) =>
                    <AboutUs3 {...item} />
                )}
              </ul>
          </div>
        </SRLWrapper >
      </SimpleReactLightbox>
    </>
  );
}
export default ServiceTemp;

